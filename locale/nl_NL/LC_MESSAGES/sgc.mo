��       3     �   G  L      h   
  i     t  m  }     �     �     �        
       )     E     N     V   K  e     �     �     �     �     �     �     	          .     J     Z     s     �     �     �   
  �     �     �     �     �   
  �   	  �     �   
  �   	                 "     1     @     S     d     l     u   
  ~     �     �  N  �     �     �  r       5u     5z     5   %  5�     5�   #  5�   
  5�   	  5�     5�   L  6     6R     6W     6[     6g     6s     6�     6�     6�   "  6�     6�     6�     7     7&     7C     7H     7_     7o     7x   	  7~     7�     7�   	  7�     7�   
  7�   	  7�     7�     7�     7�     7�     7�     8     8     8     8(   
  81     8<     8E      0                  2      *             '   +                    &      -       .            
                           "                   	   $         #       (              1                            )   !   %   ,                    /   3      Mandarin  (c) 2007 <span size="xx-large">
<b>SpeakGoodChinese
(shuo1 hao3 zhong1 wen2)</b>
</span>

<span size="x-large">
<big><b>Manual</b></big>
</span>
<span size="medium">
The SpeakGoodChinese tone recognizer tries to help you to practise to pronounce mandarin tones in a "normal" voice. It tries to recognize what tones you said assuming that you pronounced the requested words in a normal voice. It will warn you if you speak with a voice that is either too low, too high, if you exaggerate the tone melodies or reduce them too much (flat tones).
</span>
<span size="medium">
The abilities of the tone recognizer are limited. It will only work reliably if you pronounce the requested words in a normal voice. You should also practise in silent surroundings using a low noise microphone. It also helps to check the settings of the audio-mixer to ensure that your voice is neither recorded too loud (clipped sounds) or too soft (very noisy).
</span>
<span size="medium">
Recognizing errors is always difficult. SpeakGoodChinese is no exception to this rule. Therefore, accepting or rejecting your pronunciation as correct will be much more accurate than determining what exactly you did wrong. If SpeakGoodChinese informs you that it rejected your pronunciation, you should always look at the recognition result, the feedback and the graphical display of your tone contours. It also helps to look at how your tones were pronounced when they are correct.
</span>

<span size="x-large">
Display
</span>
<span size="large">
Tone contours</span>
<span size="medium">
In the top part of the display, you see a graphical model of the melody, or pitch, of the requested tones in black. After you have recorded your own pronunciation of the word, the melody of your voice is displayed in red. If your voice is to high or low, you might consider changing the base register of your voice in the Voice menu.
</span>

<span size="large">Pinyin</span>
<span size="medium">
On the display, you will find the pinyin transcription of the requested word. After you have recorded your own pronunciation, this will be replaced by the recognition result. In some cases, the tones could not be recognized, and you will see question marks (?) instead. The advice will try to explain what happened. Note that SpeakGoodChinese is better at recognizing correct tones than specifying what exactly went wrong, especially if the melody of your voice could not be determined accurately. So we advice you to always look at the red tone contour to see what happened.
</span>

<span size="large">Advice</span>
<span size="medium">
After your own speech has been recognized, you will find the result printed out below the recognition result. 
</span>

<span size="x-large">
Buttons
</span>

<span size="large">Record</span>
<span size="medium">
Record your voice. You get 2 seconds to speak the requested word. SpeakGoodChinese does not work well if there is background noise. Also, some microphones can add a lot of noise, which could interfere with recognition. The tone recognizer assumes you try to speak the requested word. If you pronounce a different word, the results are unpredictable. In some cases, a wrongly pronounced word can be recognized as correct erroneously. SpeakGoodChinese will try to isolate the word you speak from other sounds. This means that if you pause between syllables, either syllable might be cut off the word and you end up with only a single syllable. In this case, the results of the recognition will be completely random.
</span>

<span size="large">Play</span>
<span size="medium">
Replay the recorded sound.
</span>

<span size="large">Example</span>
<span size="medium">
Play an example of the requested sound. In the Play menu, you can choose whether to play hummed tones, or existing recordings.
</span>

<span size="large">Previous</span>
<span size="medium">
Go to the previous item on the list.
</span>

<span size="large">Next</span>
<span size="medium">
Go to the next item on the list
</span>

<span size="x-large">Menu</span>

<span size="large">File</span>
<span size="medium">
<i>not functional yet</i>
Quit - Stop application
</span>

<span size="large">Play</span>

<span size="medium"><u>Before</u></span>
<span size="medium">Play an example before you Record</span>
<span size="medium"><i>Voice</i> - Play an existing example</span>
<span size="medium"><i>Hum</i> - Generate a hummed contour</span>

<span size="medium"><u>Example</u></span>
<span size="medium">The example played after pushing the Example button</span>
<span size="medium"><i>Voice</i> - Play a pre-recorded example or resynthesize the last the correct tones (if available)</span>
<span size="medium"><i>Hum</i> - Generate a hummed contour</span>

<span size="large">Voice</span>
<span size="medium">You must choose what type of voice you have. Basically, if you often get "Too Low" or "Too High"  messages, you might want to raise or lower the indicated register. You can always look at the display and try to find a voice register that makes your own voice (red line) match the Reference Pitch (black line) best. The frequency (in Hz) indicated is always the ideal frequency of the first tone.</span>

<span size="medium"><i>Male (150Hz)</i> - Low range male</span>
<span size="medium"><i>Male (200Hz)</i> - Mid range male</span>
<span size="medium"><i>Female (300Hz)</i> - Mid range female</span>
<span size="medium"><i>Child (450Hz)</i> - High pitch voice, children</span>


<span size="large">Words</span>
<span size="medium">
<i>Add+</i> - Add a word to the list
(the file tests.txt)
</span>

<span size="large">Help</span>

<span size="medium"><i>Help</i> - This Menu :)</span>

<span size="medium"><i>About</i> - About the SpeakGoodChinese application</span> About Before Child (450Hz) Could not open the GUI! Distribute Don't have any (more) tests E_xample Example Female (300Hz) GNU General Public License version 2, or in your opinion any later version. Help Hum Male (150Hz) Male (200Hz) Missing recording directory %s. No sound from praat! Open Distribution Plain Text (*.txt) Praat cannot open the file! Reference Pitch SGC Distribution (*.sgc) Speak Good Chinese The file %s just don't exist! Voice Wordlist Editor Your Pitch _File _Help _Play _Voice _Wordlists gtk-about gtk-add gtk-cancel gtk-close gtk-edit gtk-help gtk-media-next gtk-media-play gtk-media-previous gtk-media-record gtk-new gtk-open gtk-quit gtk-remove gtk-save gtk-save-as Project-Id-Version: SGC 0.3a
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-06-11 01:46+0200
PO-Revision-Date: 2007-06-11 01:53+0200
Last-Translator: skinkie <(null)>
Language-Team: English <en@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);  Mandarijn  (c) 2007 <span size="xx-large">
<b>SpeakGoodChinese
(shuo1 hao3 zhong1 wen2)</b>
</span>

<span size="x-large">
<big><b>Handleiding</b></big>
</span>
<span size="medium">
The SpeakGoodChinese tone recognizer tries to help you to practise to pronounce mandarin tones in a "normal" voice. It tries to recognize what tones you said assuming that you pronounced the requested words in a normal voice. It will warn you if you speak with a voice that is either too low, too high, if you exaggerate the tone melodies or reduce them too much (flat tones).
</span>
<span size="medium">
The abilities of the tone recognizer are limited. It will only work reliably if you pronounce the requested words in a normal voice. You should also practise in silent surroundings using a low noise microphone. It also helps to check the settings of the audio-mixer to ensure that your voice is neither recorded too loud (clipped sounds) or too soft (very noisy).
</span>
<span size="medium">
Recognizing errors is always difficult. SpeakGoodChinese is no exception to this rule. Therefore, accepting or rejecting your pronunciation as correct will be much more accurate than determining what exactly you did wrong. If SpeakGoodChinese informs you that it rejected your pronunciation, you should always look at the recognition result, the feedback and the graphical display of your tone contours. It also helps to look at how your tones were pronounced when they are correct.
</span>

<span size="x-large">
Display
</span>
<span size="large">
Tone contours</span>
<span size="medium">
In the top part of the display, you see a graphical model of the melody, or pitch, of the requested tones in black. After you have recorded your own pronunciation of the word, the melody of your voice is displayed in red. If your voice is to high or low, you might consider changing the base register of your voice in the Voice menu.
</span>

<span size="large">Pinyin</span>
<span size="medium">
On the display, you will find the pinyin transcription of the requested word. After you have recorded your own pronunciation, this will be replaced by the recognition result. In some cases, the tones could not be recognized, and you will see question marks (?) instead. The advice will try to explain what happened. Note that SpeakGoodChinese is better at recognizing correct tones than specifying what exactly went wrong, especially if the melody of your voice could not be determined accurately. So we advice you to always look at the red tone contour to see what happened.
</span>

<span size="large">Advice</span>
<span size="medium">
After your own speech has been recognized, you will find the result printed out below the recognition result. 
</span>

<span size="x-large">
Buttons
</span>

<span size="large">Record</span>
<span size="medium">
Record your voice. You get 2 seconds to speak the requested word. SpeakGoodChinese does not work well if there is background noise. Also, some microphones can add a lot of noise, which could interfere with recognition. The tone recognizer assumes you try to speak the requested word. If you pronounce a different word, the results are unpredictable. In some cases, a wrongly pronounced word can be recognized as correct erroneously. SpeakGoodChinese will try to isolate the word you speak from other sounds. This means that if you pause between syllables, either syllable might be cut off the word and you end up with only a single syllable. In this case, the results of the recognition will be completely random.
</span>

<span size="large">Play</span>
<span size="medium">
Replay the recorded sound.
</span>

<span size="large">Example</span>
<span size="medium">
Play an example of the requested sound. In the Play menu, you can choose whether to play hummed tones, or existing recordings.
</span>

<span size="large">Previous</span>
<span size="medium">
Go to the previous item on the list.
</span>

<span size="large">Next</span>
<span size="medium">
Go to the next item on the list
</span>

<span size="x-large">Menu</span>

<span size="large">File</span>
<span size="medium">
<i>not functional yet</i>
Quit - Stop application
</span>

<span size="large">Play</span>

<span size="medium"><u>Before</u></span>
<span size="medium">Play an example before you Record</span>
<span size="medium"><i>Voice</i> - Play an existing example</span>
<span size="medium"><i>Hum</i> - Generate a hummed contour</span>

<span size="medium"><u>Example</u></span>
<span size="medium">The example played after pushing the Example button</span>
<span size="medium"><i>Voice</i> - Play a pre-recorded example or resynthesize the last the correct tones (if available)</span>
<span size="medium"><i>Hum</i> - Generate a hummed contour</span>

<span size="large">Voice</span>
<span size="medium">You must choose what type of voice you have. Basically, if you often get "Too Low" or "Too High"  messages, you might want to raise or lower the indicated register. You can always look at the display and try to find a voice register that makes your own voice (red line) match the Reference Pitch (black line) best. The frequency (in Hz) indicated is always the ideal frequency of the first tone.</span>

<span size="medium"><i>Male (150Hz)</i> - Low range male</span>
<span size="medium"><i>Male (200Hz)</i> - Mid range male</span>
<span size="medium"><i>Female (300Hz)</i> - Mid range female</span>
<span size="medium"><i>Child (450Hz)</i> - High pitch voice, children</span>


<span size="large">Words</span>
<span size="medium">
<i>Add+</i> - Add a word to the list
(the file tests.txt)
</span>

<span size="large">Help</span>

<span size="medium"><i>Help</i> - This Menu :)</span>

<span size="medium"><i>About</i> - About the SpeakGoodChinese application</span> Over Voor Kind (450Hz) Kan gebruikers interface niet openen! Distribueer Er zijn geen (anderen) opgaven meer _Voorbeeld Voorbeeld Vrouw (300Hz) GNU Algemene Publieke Licentie versie 2, of in uw opinie elke latere versie. Help Hum Man (150Hz) Man (200Hz) Missende opname map %s. Geen geluid uit praat! Open Distributie Platte tekst (*.txt) Praat kan het bestand niet openen! Doel Toonhoogte SGC Distributie (*.sgc) Speak Good Chinese Het bestand %s bestaat niet! Stem Woordenlijst verwerker Jouw Toonhoogte _Bestand _Help _Afspelen _Stem _Woordenlijst gtk-about gtk-add gtk-cancel gtk-close gtk-edit gtk-help gtk-media-next gtk-media-play gtk-media-previous gtk-media-record gtk-new gtk-open gtk-quit gtk-remove gtk-save gtk-save-as 