#include "sgc.h"
#include "praat.h"
#include "signals.h"

GladeXML *xml = NULL;

int main(int argc, char **argv)
{
	void * needed;
	needed = (void *)on_buttonRecord_clicked;
	needed = (void *)on_treeviewWords_cursor_changed;
	needed = (void *)on_buttonSaveDistribution_clicked;
	needed = (void *)paint;
	needed = (void *)on_menuitemVoice_activate;
	needed = (void *)on_buttonExample_clicked;
	needed = (void *)on_Save;
	needed = (void *)on_buttonSave_clicked;
	needed = (void *)on_filechooserdialog_realize;
	needed = (void *)on_treeother;
	needed = (void *)on_buttonAddAdd_clicked;
	needed = (void *)on_menuitemShuffle_activate;
	needed = (void *)remove_entry;
	needed = (void *)on_buttonPlay_clicked;
	needed = (void *)on_buttonSaveNew_clicked;
	needed = (void *)configSave;
	needed = (void *)on_menuWordlists_realize;
	needed = (void *)on_buttonOpen_clicked;

	gtk_init(&argc, &argv);

	bindtextdomain(g_get_application_name(), LOCALEDIR);

/*	textdomain(g_get_application_name());
 *
 *	This makes the application use a standard domain,
 *	instead of a changing program name. For example
 *	on win32 it will be sgc.exe
 */

	textdomain("sgc");

	xml = glade_xml_new(GLADESOURCE, NULL, g_get_application_name());

	if (xml == NULL) {
		g_error(_("Could not open the GUI!"));
		return -1;
	} else {
		if (!g_thread_supported()) {
			g_thread_init(NULL);
		}

		/* connect signal handlers */
		glade_xml_signal_autoconnect(xml);

		on_windowMain_realize(NULL, NULL);

	        g_thread_create((GThreadFunc)sound_init, NULL, FALSE, NULL);

		removeRecordings();

		gtk_main();

		removeRecordings();

		setBase(NULL);
		setFile(NULL);
		
		return 0;
	}
}
