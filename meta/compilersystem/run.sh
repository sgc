rm -r targets

mkdir -p targets/win32/pinyin
mkdir -p targets/win32/PitchTiers
mkdir -p targets/win32/distfiles
mkdir -p targets/win32/records
mkdir -p targets/linux/pinyin
mkdir -p targets/linux/PitchTiers
mkdir -p targets/linux/distfiles
mkdir -p targets/linux/records

touch targets/win32/pinyin/.keep
touch targets/win32/PitchTiers/.keep
touch targets/win32/distfiles/.keep
touch targets/win32/records/.keep
touch targets/linux/pinyin/.keep
touch targets/linux/PitchTiers/.keep
touch targets/linux/distfiles/.keep
touch targets/linux/records/.keep

cd sources

cd sgc
git pull git://fondialog1.hum.uva.nl/sgc.git
cp -r SGC_ToneProt ../../targets/win32/.
cp -r SGC_ToneProt ../../targets/linux/.
cp -r wordlists    ../../targets/win32/.
cp -r wordlists    ../../targets/linux/.
cp -r locale       ../../targets/win32/.
cp -r locale       ../../targets/linux/.
cp -r doc          ../../targets/win32/.
cp -r doc          ../../targets/linux/.

cp sgc.png COPYING README.txt  ../../targets/win32/.
cp sgc.png COPYING README.txt  ../../targets/linux/.

cd ..

cd sgc-bin
git pull git://fondialog1.hum.uva.nl/sgc-bin.git
cp -r * ../../targets/win32/.
mv ../../targets/win32/*.sh ../../targets/linux/.
mv ../../targets/win32/SGC_ToneProt/praat ../../targets/linux/SGC_ToneProt/.
cd ..

cd sgc

make -f Makefile.mingw32 clean
make -f Makefile.mingw32
cp sgc.exe sgc.glade ../../targets/win32/.
make -f Makefile.mingw32 clean

cd singleword
make -f Makefile.mingw32 clean
make -f Makefile.mingw32
cp singleword.exe singleword.glade ../../../targets/win32/.
make -f Makefile.mingw32 clean
cd ..

make -f Makefile.linux clean
make -f Makefile.linux
cp sgc sgc.glade ../../targets/linux/.
make -f Makefile.linux clean

cd singleword
make -f Makefile.linux clean
make -f Makefile.linux
cp singleword singleword.glade ../../../targets/linux/.
make -f Makefile.linux clean
cd ..

cd ..

cd ..

cd targets/win32
wine ~/.wine/drive_c/Program\ Files/NSIS/makensis.exe installer.nsi
cd ..
cp /usr/lib/libpraat.so linux/.
cd ..
