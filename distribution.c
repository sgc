#include "sgc.h"
#include <zip.h>

void saveSGC(const gchar *dir, gchar *path) {
	if (dir != NULL && path != NULL) {
		gchar *wordlist = g_build_path(G_DIR_SEPARATOR_S, path, "wordlist.txt", NULL);
//		g_debug("%s", wordlist);
		if (wordlist != NULL) {
			if (g_access(wordlist, R_OK) == 0) {
				gchar *filename = g_strjoin(NULL, dir, ".sgc", NULL);
				gchar *path    = g_build_path(G_DIR_SEPARATOR_S, g_getenv(STORAGE), STOREAS, DISTPATH, NULL);
				if (path != NULL) {
					gchar *zippath = g_build_path(G_DIR_SEPARATOR_S, path, filename, NULL);

//					g_debug("%s", zippath);

					if (zippath != NULL) {
						struct zip *za;
						int err;
						g_mkdir_with_parents(path, 0755);
						if ((za = zip_open(zippath, ZIP_CREATE, &err)) != NULL) {
							GDir *dirpath;
							if ((dirpath = g_dir_open(path, 0, NULL)) != NULL) {
								const gchar *file;
								struct zip_source *zs;
								while ((file = g_dir_read_name(dirpath)) != NULL) {
									gchar *pathtofile = g_build_path(G_DIR_SEPARATOR_S, path, file, NULL);
									if ((zs=zip_source_file(za, pathtofile, 0, -1)) != NULL) {
										zip_add(za, file, zs);
									} else {
										g_debug("%s %s", pathtofile, zip_strerror(za));
									}
									g_free(pathtofile);
								}
								g_dir_close(dirpath);
							}
							zip_close(za);
						}
						g_free(zippath);
					}
					g_free(path);
				}
				g_free(filename);
			} else 
			g_free(wordlist);
		}
	}
}

gboolean openSGC(gchar *oldfilename) {
	struct zip *za;
	int err;
	gboolean toreturn = FALSE;

	if (oldfilename == NULL || (za = zip_open(oldfilename, 0, &err)) == NULL) {
		/* Kan bestand niet openen */
	} else {
		if (zip_name_locate(za, "wordlist.txt", 0) == -1) {
			/* Geen valide bestand */
		} else {
			int i;
			struct zip_file *zf;
			struct zip_stat zs;
			gchar *data;
			gchar *name;
			gchar *base;
			gchar *filename = g_path_get_basename(oldfilename);
			int end = strlen(filename);

			if (g_str_has_suffix(filename, ".sgc") ||
					g_str_has_suffix(filename, ".SGC")) {
				end -= 3; 
			}

			name = g_malloc(end * sizeof(gchar));

			if (name != NULL) {

				g_strlcpy(name, filename, end);


				base = g_build_path(G_DIR_SEPARATOR_S, g_getenv(STORAGE), STOREAS, WORDLISTS, name, NULL);

				g_mkdir_with_parents(base, 0755);

				for (i = 0; i < zip_get_num_files(za); i++) {
					if ((zf = zip_fopen_index(za, i, 0)) != NULL &&
							(zip_stat_index(za, i, 0, &zs)) != -1) {
						data = g_malloc(sizeof(char) * zs.size);

						if (data != NULL) {
							int test;

							if ((test = zip_fread(zf, data, zs.size)) == zs.size) {
								gchar *dir = g_build_path(G_DIR_SEPARATOR_S, base, zs.name, NULL);
								g_file_set_contents(dir, data, zs.size, NULL);
								g_free(dir);
							}
							g_free(data);
						}

						zip_fclose(zf);
					}
				}
				g_free(name);
				g_free(base);
				toreturn = TRUE;
			}
			g_free(filename);
		}
		zip_close(za);
	}
	return toreturn;
}
