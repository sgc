#include "sgc.h"
#include "signals-editor.h"

gchar * checkPinyin(gchar *pinyin) {
        gchar **result;
        g_ascii_strdown(pinyin, -1);
        g_strcanon(pinyin, "#012345abcdefghijklmnopqrstuvwxyz\n", '-');
        result = g_strsplit(pinyin, "-", MAXTESTS * 2);
        g_free(pinyin);
        pinyin = g_strjoinv(NULL, result);
        return pinyin;
}

void on_buttonAddAdd_clicked (GtkWidget *entry, gpointer data)
{
	gchar *processed = checkPinyin(g_strdup(gtk_entry_get_text(GTK_ENTRY(entry))));
	add_entry(processed);
}

void add_entry(gchar *txt) {
	/* ignore if entry is empty */
	if (txt && *txt)
	{
		GtkTreeIter   newrow;

		gtk_list_store_append(GTK_LIST_STORE(liststore), &newrow);

		gtk_list_store_set(GTK_LIST_STORE(liststore), &newrow, COL_TEXT, txt, -1);
	//	g_free(txt);

		if (treevalid == FALSE) {
			treevalid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(liststore), &mainIter);
		}
		updateWidgets();
	}
}

void remove_entry (GtkWidget *treeview, gpointer data)
{
	GtkTreeSelection *sel;
	GtkTreeIter       selected_row;

	sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));

	if (gtk_tree_selection_get_selected(sel, (GtkTreeModel **) &liststore, &selected_row))
	{
		gtk_list_store_remove(GTK_LIST_STORE(liststore), &selected_row);
		treevalid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(liststore), &mainIter);
		updateWidgets();
	}
}

void on_treeviewWords_cursor_changed(GtkTreeView *treeview, gpointer user_data) {
	GtkTreeSelection *sel;
	sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	
	if (gtk_tree_selection_get_selected(sel, (GtkTreeModel **)&liststore, &mainIter)) {
		updateWidgets();
	}
}

void on_treeview_edited (GtkCellRendererText *celltext,
		const gchar *string_path, const gchar *new_text, gpointer data) {

	GtkTreeModel *model = GTK_TREE_MODEL(data);
	GtkTreeIter   iter;
	gtk_tree_model_get_iter_from_string(model, &iter, string_path);
	gtk_list_store_set(GTK_LIST_STORE(model), &iter, COL_TEXT, new_text, -1);
	treevalid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(liststore), &mainIter);
	updateWidgets();
}

gboolean on_treeother() {
	treevalid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(liststore), &mainIter);
	updateWidgets();
	return false;
}

void on_buttonSave_clicked(GtkWidget *filechooser, gpointer data) {
	gchar *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooser));
	fileSave(filename);
	fileOpen(filename);
	g_free(filename);
}


void on_buttonOpen_clicked(GtkWidget *filechooser, gpointer data) {
	/* Eerst vragen oude op te slaan ? */
	
	gchar *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooser));

	if (g_str_has_suffix(filename, ".sgc") || g_str_has_suffix(filename, ".SGC") || g_str_has_suffix(filename, ".zip") || g_str_has_suffix(filename, ".ZIP")) {
		if (openSGC(filename)) {
			gchar *basename = g_path_get_basename(filename);
			int end = strlen(basename)-3;
			gchar *basenamenoext = g_malloc(end * sizeof(gchar));
			gchar *currentDir = g_get_current_dir();
	                g_strlcpy(basenamenoext, basename, end);
			g_free(filename);
			filename = g_strjoin(NULL, currentDir, G_DIR_SEPARATOR_S, WORDLISTS, G_DIR_SEPARATOR_S, basenamenoext, G_DIR_SEPARATOR_S, "wordlist.txt", NULL);
			g_free(currentDir);
			g_free(basename);
			g_free(basenamenoext);
			gtk_widget_unrealize(glade_xml_get_widget(xml, "menuWordlists"));
		}
	}
	fileOpen(filename);
	g_free(filename);
}

void on_buttonSaveDistribution_clicked(GtkWidget *entry, gpointer data) {
	if (G_OBJECT_TYPE(entry) == GTK_TYPE_ENTRY) {
		saveSGC(gtk_entry_get_text(GTK_ENTRY(entry)), base);
	}
}
void on_buttonSaveNew_clicked(GtkWidget *entry, gpointer data) {
	if (G_OBJECT_TYPE(entry) == GTK_TYPE_ENTRY) {
		gchar *currentDir = g_get_current_dir();
		gchar *filename = g_strjoin(NULL, currentDir, G_DIR_SEPARATOR_S, WORDLISTS, G_DIR_SEPARATOR_S, gtk_entry_get_text(GTK_ENTRY(entry)), G_DIR_SEPARATOR_S, "wordlist.txt", NULL);
		clear(NULL);
		setBase(g_path_get_dirname(filename));
		setFile(filename);
		if (g_access(base, F_OK) != 0) {
			g_mkdir_with_parents(base, 0755);
		}

		if (g_access(file, F_OK) == 0) {
			fileOpen(file);
		} else {
			gtk_window_set_title(GTK_WINDOW(glade_xml_get_widget(xml, "windowWordlist")), file);
			gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(glade_xml_get_widget(xml, "filechooserdialogWordlist")), file);
			fclose(g_fopen(file, "w"));
			gtk_widget_unrealize(glade_xml_get_widget(xml, "menuWordlists"));
		}
	}
}
