#include "sgc.h"
#include "signals.h"

gdouble upperRegister = 200.0;

gchar * voicepath(gchar *string) {
	gchar *path = g_strjoin(NULL, base, G_DIR_SEPARATOR_S, string, ".wav", NULL);
	if (g_access(path, F_OK) != 0) {
		g_free(path);
		path = g_strjoin(NULL, g_getenv(STORAGE), G_DIR_SEPARATOR_S, STOREAS, G_DIR_SEPARATOR_S, PINYINPATH, G_DIR_SEPARATOR_S, string, ".wav", NULL);
	}
	return path;
}

static void setButtonByFileExists(GtkWidget *button, gchar *path) {
	if (g_access(path, F_OK) == 0) {
		gtk_widget_set_sensitive(button, TRUE);
	} else {
		gtk_widget_set_sensitive(button, FALSE);
	}
}


static void setButtonExample() {
	GtkWidget *buttonExample = glade_xml_get_widget(xml, "buttonExample");
	if (treevalid) {
		GtkWidget *gimiHum = glade_xml_get_widget(xml, "checkmenuitemExampleHum");
		if (!gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(gimiHum))) {
			gchar *string;
			gchar *path;

			gtk_tree_model_get(GTK_TREE_MODEL(liststore), &mainIter, COL_TEXT, &string, -1);
			path = voicepath(string);

			setButtonByFileExists(buttonExample, path);

			g_free(path);
			g_free(string);
		} else {
			gtk_widget_set_sensitive(buttonExample, TRUE);
		}
	} else {
		gtk_widget_set_sensitive(buttonExample, FALSE);
	}
}

gchar * pathToPlay() {
	gchar *path = NULL;
#ifdef LASTEXAMPLE
	path = g_build_filename(SCRIPTPATH, "lastExample.wav", NULL);
#else
	if (treevalid) {
		gchar *string;
		GString *filename = g_string_sized_new(128);
		gtk_tree_model_get(GTK_TREE_MODEL(liststore), &mainIter, COL_TEXT, &string, -1);
		g_string_printf(filename, "%s.wav", string);
		path = g_build_filename(RECORDPATH, filename->str, NULL);
		g_string_free(filename, TRUE);
	}
#endif
	return path;
}

static void setButtonPlay() {
	GtkWidget *buttonPlay = glade_xml_get_widget(xml, "buttonPlay");
	gchar *path = pathToPlay();
	if (path != NULL) {
		setButtonByFileExists(buttonPlay, path);
		g_free(path);
	}
}

gboolean setButtonsTrue() {
	GtkWidget *buttonRecord = glade_xml_get_widget(xml, "buttonRecord");

	GtkWidget *drawingareaPitch = glade_xml_get_widget(xml, "drawingareaPitch");
	gchar *feedbackTXT;
	gchar *contents;


	gtk_widget_set_sensitive(buttonRecord, TRUE);
	setButtonPlay();
	setButtonExample();

	gtk_widget_queue_draw(drawingareaPitch);

	feedbackTXT = g_build_filename(SCRIPTPATH, G_DIR_SEPARATOR_S, "feedback.txt", NULL);

	if (g_file_get_contents(feedbackTXT, &contents, NULL, NULL) == TRUE) {
		//		g_debug("Sets label!\n");
		GtkWidget *label = glade_xml_get_widget(xml, "labelFeedback");
		gtk_label_set_text(GTK_LABEL(label), contents);
		//		g_debug("Label set!\n");
		g_unlink(feedbackTXT);
	}
	g_free(feedbackTXT);

	return FALSE;
}

gboolean setButtonsFalse() {
	GtkWidget *buttonRecord = glade_xml_get_widget(xml, "buttonRecord");
	GtkWidget *buttonPlay = glade_xml_get_widget(xml, "buttonPlay");
	GtkWidget *buttonExample = glade_xml_get_widget(xml, "buttonExample");
	//	g_debug("%s welcome!\n", __func__);

	gtk_widget_set_sensitive(buttonRecord, FALSE);
	gtk_widget_set_sensitive(buttonPlay, FALSE);
	gtk_widget_set_sensitive(buttonExample, FALSE);

	//	g_debug("%s bye!\n", __func__);

	return FALSE;
}

void on_buttonRecord_clicked(GtkWidget *widget, gpointer user_data) {
	if (treevalid) {
		gchar *string;
		gtk_tree_model_get(GTK_TREE_MODEL(liststore), &mainIter, COL_TEXT, &string, -1);


		if (string == NULL) {
			GtkWidget *dialog = gtk_message_dialog_new(NULL,
					GTK_DIALOG_MODAL,
					GTK_MESSAGE_WARNING,
					GTK_BUTTONS_OK,
					_("Don't have any (more) tests"));
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog);
			/* Nothing to do? */
		} else {
			/* Start record Thread */

			//			g_debug("%s creates record", __func__);

			g_thread_create((GThreadFunc)record, NULL, FALSE, NULL);

			//			g_debug("%s is done", __func__);
			g_free(string);
		}
	}
}

void on_buttonExample_clicked(GtkWidget *widget, gpointer user_data) {
	/* Play Thread */
	/* Start example Thread */
	g_thread_create((GThreadFunc)example, widget, FALSE, NULL);
}

void on_buttonPlay_clicked(GtkWidget *widget, gpointer user_data) {
	/* Play Thread */
	/* Start play Thread */
	g_thread_create((GThreadFunc)play, widget, FALSE, NULL);
}

gboolean prev(GtkWidget *previous) {
	gboolean ret = FALSE;
	if (treevalid) {
		/* Code From anchient Sylpheed */
		GtkTreePath *path;
		GtkTreeIter prev;

		path = gtk_tree_model_get_path(GTK_TREE_MODEL(liststore), &mainIter);

		if ((ret = gtk_tree_path_prev(path)) == TRUE) {
			gtk_tree_model_get_iter(GTK_TREE_MODEL(liststore), &prev, path);
			if (previous != NULL) {
				mainIter = prev;
			}

		}

		gtk_tree_path_free(path);

	}

	return ret;
}

gboolean next(GtkWidget *next) {
	if (treevalid) {
		if (next != NULL) {
			return gtk_tree_model_iter_next(GTK_TREE_MODEL(liststore), &mainIter);
		} else {
			GtkTreeIter next = mainIter;
			return gtk_tree_model_iter_next(GTK_TREE_MODEL(liststore), &next);
		}
	} else {
		return FALSE;
	}
}

void updateWidgets() {
	GtkWidget *labelFeedback = glade_xml_get_widget(xml, "labelFeedback");

	if (treevalid) {
		gchar *string;
		gtk_tree_model_get(GTK_TREE_MODEL(liststore), &mainIter, COL_TEXT, &string, -1);
		GString *label = g_string_new(string);

		g_string_prepend(label, "<span size=\"xx-large\">");
		g_string_append(label, "</span>");
		gtk_label_set_markup(GTK_LABEL(labelFeedback), label->str);
		g_string_free(label, TRUE);

		g_free(string);
		gtk_tree_selection_select_iter(gtk_tree_view_get_selection(GTK_TREE_VIEW(glade_xml_get_widget(xml, "treeviewWords"))), &mainIter);
	} else {
		gtk_label_set_markup(GTK_LABEL(labelFeedback), "");
	}

	gtk_widget_set_sensitive(glade_xml_get_widget(xml, "buttonRecord"), treevalid);
	gtk_widget_set_sensitive(glade_xml_get_widget(xml, "imagemenuitemSave"), treevalid);
	gtk_widget_set_sensitive(glade_xml_get_widget(xml, "imagemenuitemSaveAs"), treevalid);

	gtk_widget_set_sensitive(glade_xml_get_widget(xml, "buttonPrevious"), prev(NULL));
	gtk_widget_set_sensitive(glade_xml_get_widget(xml, "buttonNext"), next(NULL));

	setButtonPlay();
	setButtonExample();

	gtk_widget_queue_draw(glade_xml_get_widget(xml, "drawingareaPitch"));
}

void on_Save(GtkWidget *chooser, gpointer user_data) {
	fileSave(gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser)));
}

void on_dialogAdd_close(GtkWidget *button, gpointer user_data) {
	GtkWidget *buttonAdd = glade_xml_get_widget(xml, "buttonAdd");
	gtk_widget_hide(gtk_widget_get_toplevel(button));
	gtk_widget_grab_focus(buttonAdd);
}

void on_menuitemVoice_activate(GtkWidget *widget, gpointer user_data) {
	const gchar *name = gtk_widget_get_name(widget);
	GtkWidget *drawingareaPitch = glade_xml_get_widget(xml, "drawingareaPitch");
	if (g_str_equal(name, "menuitemVoice_Male_Low") == TRUE) upperRegister = 150.0;
	if (g_str_equal(name, "menuitemVoice_Male") == TRUE) upperRegister = 200.0;
	if (g_str_equal(name, "menuitemVoice_Female") == TRUE) upperRegister = 300.0;
	if (g_str_equal(name, "menuitemVoice_Child") == TRUE) upperRegister = 450.0;
	gtk_widget_queue_draw(drawingareaPitch);
}

void paint (GtkWidget *widget, GdkEventExpose *eev, gpointer data) {
	if (treevalid) {
		gchar *string;
		gtk_tree_model_get(GTK_TREE_MODEL(liststore), &mainIter, COL_TEXT, &string, -1);

		if (string != NULL) {
			cairo_t *cr;
			GString *filename = g_string_sized_new (200);
			GString *cmd = g_string_sized_new (200);
			GString *recordedPT = g_string_sized_new(100);

#ifdef PITCHTIER
			g_string_printf(filename, "%s%s%s-%0.f.PitchTier", PITCHPATH, G_DIR_SEPARATOR_S, string, upperRegister);
			g_string_printf(recordedPT, "%s%s%s.PitchTier", RECORDPATH, G_DIR_SEPARATOR_S, string);
#else
			g_string_printf(filename, "%s%s%s-%0.f.Pitch", PITCHPATH, G_DIR_SEPARATOR_S, string, upperRegister);
			g_string_printf(recordedPT, "%s%s%s.Pitch", RECORDPATH, G_DIR_SEPARATOR_S, string);	
#endif

			if (g_access(filename->str, F_OK) == -1) {
				g_chdir(SCRIPTPATH);
#ifdef PRAATEXTERNAL
				g_string_printf(cmd, "%s DrawToneContour.praat %s %0.f", PRAATBIN, string, upperRegister);
//				g_debug("%s", cmd->str);
				system(cmd->str);
#else
				g_string_printf(cmd, "DrawToneContour.praat %s %0.f", string, upperRegister);
				praat_executeScriptFromFileNameWithArguments(cmd->str);
		                Melder_clearError();
#endif
				
				g_chdir("..");
			}

			cr = gdk_cairo_create (widget->window);
			cairo_set_line_width (cr, 1);
			cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
					CAIRO_FONT_WEIGHT_BOLD);


			if (g_access(filename->str, F_OK) == 0) {
				cairo_move_to (cr, 5, 15);
				cairo_set_source_rgb (cr, 0,0,0);
				cairo_show_text (cr, _("Reference Pitch"));


				drawPitchTier(cr, filename->str,
						widget->allocation.width,
						widget->allocation.height, upperRegister + 100.0);
			}
			g_string_free(filename, TRUE);
			// g_debug("%s", recordedPT->str);

			if (g_access(recordedPT->str, F_OK) == 0) {
				cairo_move_to (cr, 15, 30);
				cairo_set_source_rgb (cr, 1,0,0);
				cairo_show_text (cr, _("Your Pitch"));

				drawPitchTier(cr, recordedPT->str,
						widget->allocation.width,
						widget->allocation.height, upperRegister + 100.0);

			}
			g_string_free(recordedPT, TRUE);
			cairo_destroy (cr);
		}
		g_free(string);
	}
}

void on_menuitemShuffle_activate(GtkWidget *menuitem, gpointer data) {
	int i;
	GtkTreeIter a, b;
	gint amount = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(liststore), NULL) - 1;
	if (amount > 0) {
		for (i = 0; i <= amount; i++) {
			gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(liststore),
					&a, NULL,
					g_random_int_range(0, amount));
			gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(liststore),
					&b, NULL,
					g_random_int_range(0, amount));
			gtk_list_store_swap(GTK_LIST_STORE(liststore), &a, &b);
		}
		gtk_tree_model_get_iter_first(GTK_TREE_MODEL(liststore), &mainIter);
		updateWidgets();
	}
}

