SpeakGoodChinese

All source code in this directory is licensed under the General Public License
version 2 or at your wish, any later version. Stefan de Konink, after this,
the author, did his best to create a bug free program. This program uses the
Praat program extensively.

This program should work on any platform Praat and GTK are supported on :)


The contents in the directory pinyin and wordlists CAN BE licensed under 
another license than the GPL. These contents are not required for operation.


Authors:		License: 
Praat           	GPL		- Paul Boersma/David Weenink
SGC_ToneProt    	GPL		- Rob van Son
SpeakGoodChinese	GPL		- Stefan de Konink
GtkTreeview Tutorial	GPL		-
libzip			BSD-like	- Dieter Baron/Thomas Klausner

We would like to thank Paul Boersma (UvA) for his help with Praat linking and
Tristian van Berkom for additional help with the localization code.
J.E. Raaijmakers, R. van Rijsselt and S. Verhagen for feedback on the GUI and 
Praat library integration on Windows.

Requirements
Currently the Linux version depends on Glade, Gtk+, and libzip.
These must be installed before you can compile or use SpeakGoodChinese


Collecting usage and perfomance data, and creating example audio

It is possible to record all utterances and write the recognition
results to a logFile.txt file. This option is not available from
the GUI. Currently, it is switched on by creating a file
in the SGC_ToneProt/log directory with the name logPerformance.txt 
(you can rename an existing stub file DoNotlogPerformance.txt to
logPerformance.txt). As long as there exists a file 
SGC_ToneProt/log/logPerformance.txt, every processed utterance
(audio and result) is stored in the directory SGC_ToneProt/log


Links:
http://www.praat.org/
http://www.fon.hum.uva.nl/
http://www.speechminded.com/
http://www.speakgoodchinese.org/
http://www.glade.gnome.org/
http://www.nih.at/libzip/

---------------------------------------------------------

The SpeakGoodChinese tone recognizer (SGC) helps you to practise pronouncing Mandarin Chinese tones.

Features

- The tone recogniser analyses your pronunciation of the Chinese tones by using techniques in speech recognition.

- It shows you via a graphical presentation the difference between your pronunciation of the tones and the standard pronunciation.

- It also gives you a written analysis of your pronunciation.

- You can listen to a hummed example of a tone or tone combination, before you record your pronunciation.

- You can listen to your own recorded pronunciation.

- You can practise all one or two syllabic words of Mandarin Chinese by simply adding them to the wordlist yourself. The wordlist is then saved automatically for your future visits.


STRUCTURE

SGC consists of four major subsystems: 
- Glade user interface definition (glade.gnome.org), licensed under the GNU GPL
- Praat phonetics, speech science, function library (www.praat.org), licensed under the GNU GPL
- SGC_ToneProt tone recognizer (www.SpeakGoodChinese.org), licensed under the GNU GPL
- Wordlists, combinations of wordlists and sound examples, 
  licensed under the GNU GPL and Creative Commons licenses
  (also user supplied)
SGC should run on all platforms that support both Praat and Glade. However, not all
platforms have been tested yet.
  
- GLADE
Quote from the website glade.gnome.org:

    Glade is a RAD tool to enable quick & easy development of user interfaces 
    for the GTK+ toolkit and the GNOME desktop environment, released under 
    the GNU GPL License.

    The user interfaces designed in Glade are saved as XML, and by using the 
    libglade library these can be loaded by applications dynamically as needed.

    By using libglade, Glade XML files can be used in numerous programming 
    languages including C, C++, Java, Perl, Python, C#, Pike, Ruby, Haskell, 
    Objective Caml and Scheme. Adding support for other languages is easy too.
    
The GUI definition is specified in the file sgc.glade, with additional components
in sgc.glade.h, sgc.h, and the po and locale directories. These files can be used 
with the conventional RAD tools for Glade. Definition and program code are fully 
separated, except for a file that contains the glue subroutine calls.

- PRAAT
The Praat program is originally a stand alone program for phonetics, speech, 
and acoustic research. Praat runs on most Unix variants, MacOS X and earlier 
versions of the MacOS (the latter are not maintained), Linux, and MS Windows.
SGC uses a library, libpraat, constructed from the Praat sources. SGC also 
makes extensive use of Praat script files (with the extension .praat). Praat 
is comprehensively documented at www.praat.org.

- SGC_ToneProt
The SGC_ToneProt directory contains an autonomous tone recognition module.
It consists of praat and sendpraat executables for several platforms. The
recognizer is build from platform independend praat scripts. 
The SGC GUI controls the recognizer by executing praat scripts on the 
praat executable in the background. Results are returned as files. 
SGC_ToneProt scripts perform several types of actions beside the
pure tone recognition.

- SINGLEWORD
The singleword directory contains an add-on module for SpeakGoodChinese
it makes it easy to test your own words and uses the configuration of
the main program.

- WORDLISTS
Users can add their own word lists to practise. SGC stores these lists
in the wordlists directory. SGC comes with precompiled word lists, some 
with prerecorded examples for all words. Wordlists are distributed
as files with the .sgc extension.
Word list distributions are simple ZIP files with the name <list
name>.sgc. They contain a list of all the words in pinyin with the
name wordlist.txt with one word per line (only ASCII characters) and
optionally a sound file for each word in the list. The sound files
should be named <pinyin word>.ext, where <pinyin word> is the pinyin
transcription, eg, sheng1zi4, and ext the sound extension type (eg,
wav). Note that SpeakGoodChinese uses Praat to process the sound
files. So only those sound files recognized by Praat can be used. This
excludes compressed formats like Ogg Vorbis and MP3 (see Praat: Read
from file...). Don't forget to include a LICENSE.txt file with the
copyright and licensing information. If you use one of the Creative
Commons licenses or the GNU GPL, you can ask us to put your list on
our web-site.


COMPONENT LIST

- DIRECTORIES
locale             : Localizations of the interface
pinyin             : Some standard sound files (deprecated)
PitchTiers         : Storage of Pitch and PitchTier files
po                 : Pango message files
records            : Stores recorded PitchTiers (deprecated)
todo               : Incomplete files on To Do list
wordlists          : Wordlists and examples (*.sgc files)
SGC_ToneProt       : The tone recognizer (separate subsystem)
singleword         : Add on module to test your own words

- TEXT FILES
COPYING            : GPL License
HOWTO              : Installation instructions
README.txt         : This file
TODO.txt           : List of things to do

- C CODE AND HEADER FILES
cairo.c            : The graphical module
config.c           : Handles storing and reading the config (sgc.ini) file.
distribution.c     : Handles storing and reading distribution files.
file.c             : Handles storing and reading wordlist files.
frompraat.c        : Contains modified Praat functions for drawing the Pitch.
frompraat.h        : Subroutine definitions for frompraat.c (header file)
praat.c            : All praat related calls such as sound and processing.
main.c             : The main program, just a call to Glade.
sgc.glade.h        : Text definitions of Glade (header file)
sgc.glade.h        : Text definitions of Glade (header file)
sgc.h              : Subroutine definitions for Glade and Praat (header file)
                   : And contains compiletime config.

Glade function definitions
signals.c          : GUI application logic code.
signals-editor.c   : GUI-editor application logic code.
signals-realize.c  : GUI startup logic.


- LIBRARY AND RELATED FILES
libpraat.def       : Libpraat definition file
libpraat.dll       : Windows praat library

- MAKE FILES
Makefile           : Link to the makefile used
Makefile.linux     : Makefile for Linux (on x86/64)
Makefile.linuxppc  : Makefile for Linux on PowerPC
Makefile.mingw32   : Makefile for MS Windows (XP)

- SCRIPTS AND ADDITIONAL FILES
production         : Script to copy all libraries
sgc.glade          : Glade interface definition, ie, the GUI
sgc-logo.png       : Picture of 'shuo1 hao3 zong1wen2' in characters
sgc-mounded.xar    : Vector graphics file (Xara) of the final logo
sgc.png            : SGC logo rasterized
sgc.xar            : Vector graphics file (Xara) of the design logo

- SINGLE WORD FILES
cairo.c		   : The graphical module to draw Pitch
config.c	   : A limited configuration parser for sgc.ini
config.h	   : The headerfile for config.c
main.c		   : The main routines and initialization
praat.c		   : Functionals calling praat
praat.h		   : The headerfile for praat.c
signals.c	   : The functions from the GUI
singleword.glade   : The GUI
singleword.h	   : The main header file

