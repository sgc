#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib/gstdio.h>
#include <pango/pango-utils.h>
#include <unistd.h>
#include "Thing.h"
#include "PitchTier.h"
#include "Pitch.h"

/* Paths to files and directories */
#define DISTPATH    "distfiles"    /* This is the output direcory for new distfiles */
#define LOCALEDIR   "locale"       /* This is the directory for localizations       */
#define PINYINPATH  "pinyin"       /* Here we store the common audioexamples        */ 
#define PITCHPATH   "PitchTiers"   /* This is the temporary output for praatscripts */
#define RECORDPATH  "records"      /* Here we store the voice recordings            */
#define GLADESOURCE "sgc.glade"    /* Filename that stores the Glade GUI            */
#define CONFIGFILE  "sgc.ini"      /* Filename that stores the configuration file   */
#define SCRIPTPATH  "SGC_ToneProt" /* This is the recognizer path for SGC           */
#define WORDLISTS   "wordlists"    /* This is the directory with wordlists          */
#define TESTFILE    "tests.txt"    /* If everything fails use this filename         */

#ifdef MINGW
#define STORAGE "APPDATA"
#define STOREAS "sgc"
#else
#define STORAGE "HOME"
#define STOREAS ".sgc"
#endif

/* Hardcodec configs */
#define MAXTESTS    0              /* Maximum amount of word splits from a file     */

#define RECORDTIME  3.5		   /* Amount of time to record			    */
// #define LASTEXAMPLE 1	   /* Play last example, instead of last recording  */

/* Use our own PitchTier Parser, or use the one that was copied from praat.
 * On windows we use this one because of compatibility issues
 */
#define PITCHTIER 1

#define PRAATEXTERNAL 1

/* Which binary do we need to invoke in order to start a script */
#ifdef MINGW
#define PRAATBIN "praatcon.exe"
#elif PPC
#define PRAATBIN "./praat.ppc"
#else
#define PRAATBIN "./praat"
#endif

extern GladeXML *xml;
extern gdouble upperRegister;

gpointer sound_init(void *args);
gpointer play(void *args);
gpointer example(void *args);
gpointer record(void *args);

void drawPitchTier(cairo_t *cr, gchar *filename, gint width, gint height, gdouble top);

void on_buttonNext_clicked(GtkWidget *widget, gpointer user_data);
void on_treeview_edited (GtkCellRendererText *celltext, const gchar *string_path, const gchar *new_text, gpointer data);
void on_windowMain_realize(GtkWidget *widget, gpointer user_data);
void on_treeviewWords_cursor_changed(GtkTreeView *treeview, gpointer user_data);
void on_buttonAddAdd_clicked (GtkWidget *entry, gpointer data);
void on_buttonSave_clicked(GtkWidget *filechooser, gpointer data);
void on_buttonOpen_clicked(GtkWidget *filechooser, gpointer data);
void on_buttonSaveDistribution_clicked(GtkWidget *entry, gpointer data);
void on_buttonSaveNew_clicked(GtkWidget *entry, gpointer data);
void on_filechooserdialog_realize(GtkWidget *widget, gpointer user_data);
gboolean on_treeother();
void remove_entry (GtkWidget *treeview, gpointer data);
void on_menuWordlists_realize(GtkWidget *menu, gpointer user_data);

void create_list_view();

void add_entry(gchar *txt);

void fileOpen(gchar *filename);
void fileSave(gchar *filename);
void fileClose();
void updateWidgets();

enum
{
       COL_TEXT = 0,
       NUM_COLS
};


GtkListStore       *liststore;
GtkTreeIter mainIter;
gboolean treevalid;
gchar *base;
gchar *file;

void clear();
gboolean next(GtkWidget *next);
gboolean prev(GtkWidget *prev);
void removeRecordings();
void setBase(gchar *newbase);
void setFile(gchar *newfile);

void configOpen();
void configSave();

gboolean openSGC(gchar *oldfilename);
void saveSGC(const gchar *dir, gchar *path);
