#include "sgc.h"

/* This file contains all code that is required to initiate the widgets. 
 * And should be linked to from Glade
 */

void on_filechooserdialog_realize(GtkWidget *widget, gpointer user_data){
	GtkFileFilter *filter;

	filter = gtk_file_filter_new ();
	gtk_file_filter_add_pattern (filter, "*.sgc");
	gtk_file_filter_add_pattern (filter, "*.txt");
	gtk_file_filter_add_pattern (filter, "*.zip");
	gtk_file_filter_set_name (filter, _("All Loadable Files"));
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(widget), filter);

	filter = gtk_file_filter_new ();
	gtk_file_filter_add_pattern (filter, "*.sgc");
	gtk_file_filter_set_name (filter, _("SGC Distribution (*.sgc)"));
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(widget), filter);

	filter = gtk_file_filter_new ();
	gtk_file_filter_add_pattern (filter, "*.txt");
	gtk_file_filter_set_name (filter, _("Plain Text (*.txt)"));
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(widget), filter);

	filter = gtk_file_filter_new ();
	gtk_file_filter_add_pattern (filter, "*.zip");
	gtk_file_filter_set_name (filter, _("Zip File (*.zip)"));
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(widget), filter);
}


void adder(gchar *dir, GtkWidget *menu) {
	GDir *dirExamples = g_dir_open(dir, 0, NULL);
	if (dirExamples != NULL) {
		const gchar *current;
		while ((current = g_dir_read_name (dirExamples)) != NULL) {
			gchar *path = g_build_path(G_DIR_SEPARATOR_S, dir, current, "wordlist.txt", NULL);
			#if DEBUG
				g_debug("%s", path);
			#endif
			if (g_file_test(path, G_FILE_TEST_EXISTS) == TRUE) {
				GtkWidget *item = gtk_image_menu_item_new_with_label(g_strdup(current));
				gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), gtk_image_new_from_stock(GTK_STOCK_OPEN, GTK_ICON_SIZE_MENU));
				g_signal_connect_data(item, "activate", G_CALLBACK(fileOpen), path, (GClosureNotify)g_free, G_CONNECT_SWAPPED);
				gtk_menu_shell_prepend (GTK_MENU_SHELL(menu), item);
			}
		}
		gtk_widget_show_all(GTK_WIDGET(menu));
	}
}

void on_menuWordlists_realize(GtkWidget *menu, gpointer user_data) {
	gchar *dir, *cwd;
	dir = g_build_path(G_DIR_SEPARATOR_S, g_getenv(STORAGE), STOREAS, WORDLISTS, NULL);
	adder(dir, menu);
	free(dir);
	cwd =  g_get_current_dir();
	dir = g_build_path(G_DIR_SEPARATOR_S, cwd, WORDLISTS, NULL);
	free(cwd);
	adder(dir, menu);
	free(dir);
}

void on_windowMain_realize(GtkWidget *widget, gpointer user_data) {
        GtkCellRenderer    *renderer;
        GtkTreeViewColumn  *col;
        GtkTreeSelection   *sel;
	GtkWidget	   *view = glade_xml_get_widget(xml, "treeviewWords");

        liststore = gtk_list_store_new(NUM_COLS, G_TYPE_STRING); /* NUM_COLS = 1 */

        gtk_tree_view_set_model(GTK_TREE_VIEW(view), GTK_TREE_MODEL(liststore));

        renderer = gtk_cell_renderer_text_new();
	
        g_object_set(renderer, "editable", TRUE, NULL);
        g_signal_connect(renderer, "edited", G_CALLBACK(on_treeview_edited),  GTK_TREE_MODEL(liststore));

        col = gtk_tree_view_column_new();

        gtk_tree_view_column_pack_start(col, renderer, TRUE);
        gtk_tree_view_column_add_attribute(col, renderer, "text", COL_TEXT);
        gtk_tree_view_column_set_title(col, _(" Mandarin "));

        gtk_tree_view_append_column(GTK_TREE_VIEW(view), col);

        sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(view));
        gtk_tree_selection_set_mode(sel, GTK_SELECTION_SINGLE);

	configOpen();
}

void removeRecordings() {
	GDir *record;
	gchar *file = g_build_filename(SCRIPTPATH, "lastExample.wav", NULL);
	g_unlink(file);
	g_free(file);
	if ((record = g_dir_open(RECORDPATH, 0, NULL)) != NULL) {
		const gchar *name;
		while ((name = g_dir_read_name(record)) != NULL) {
			if (g_str_has_suffix(name, ".wav") || g_str_has_suffix(name, ".Pitch") || g_str_has_suffix(name, ".PitchTier")) {
				file = g_build_filename(RECORDPATH, name, NULL);
				g_unlink(file);
				g_free(file);
			}
		}
		g_dir_close(record);
	}
}
