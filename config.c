#include "sgc.h"

void configOpen() {
	GKeyFile *config = g_key_file_new();
	if (g_key_file_load_from_file(config, CONFIGFILE, G_KEY_FILE_NONE, NULL)) {
		gchar *last;
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "checkmenuitemBeforeHum")), g_key_file_get_boolean(config, "before", "hum", NULL));
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "checkmenuitemBeforeVoice")), g_key_file_get_boolean(config, "before", "voice", NULL));
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "checkmenuitemExampleHum")), g_key_file_get_boolean(config, "example", "hum", NULL));
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "checkmenuitemExampleVoice")), g_key_file_get_boolean(config, "example", "voice", NULL));
		
		upperRegister = g_key_file_get_double(config, "voice", "upperregister", NULL);

		if (upperRegister <= 0.0) upperRegister = 200.0;

		switch ((int)upperRegister) {
			case 150:
				gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "menuitemVoice_Male_Low")), TRUE);
				break;
			case 200:
				gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "menuitemVoice_Male")), TRUE);
				break;
			case 300:
				gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "menuitemVoice_Female")), TRUE);
				break;
			case 400:
				gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "menuitemVoice_Child")), TRUE);
				break;
		}

		if ((last = g_key_file_get_string(config, "wordlist", "last", NULL)) == NULL || (g_access(last, F_OK) != 0)) {
			gchar *currentDir = g_get_current_dir();
			g_free(last);
			last = g_build_filename(currentDir, TESTFILE, NULL);
			fileOpen(last);
			fileSave(last);
			g_free(currentDir);
		}

		fileOpen(last);
		gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(glade_xml_get_widget(xml, "filechooserdialogWordlist")), last);
		g_free(last);
	}
	g_key_file_free(config);
}

void configSave() {
	GString *output;
	GKeyFile *config = g_key_file_new();
	g_key_file_load_from_file(config, CONFIGFILE, G_KEY_FILE_KEEP_COMMENTS|G_KEY_FILE_KEEP_TRANSLATIONS, NULL);
	g_key_file_set_boolean(config, "before", "hum", gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "checkmenuitemBeforeHum"))));
	g_key_file_set_boolean(config, "before", "voice", gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "checkmenuitemBeforeVoice"))));
	g_key_file_set_boolean(config, "example", "hum", gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "checkmenuitemExampleHum"))));
	g_key_file_set_boolean(config, "example", "voice", gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(glade_xml_get_widget(xml, "checkmenuitemExampleVoice"))));
	g_key_file_set_string(config, "wordlist", "last", file); 

	g_key_file_set_double(config, "voice", "upperregister", upperRegister);

	output = g_string_new(g_key_file_to_data(config, NULL, NULL));
	g_file_set_contents(CONFIGFILE, output->str, output->len, NULL);
	
	g_string_free(output, TRUE);
	g_key_file_free(config);
}
