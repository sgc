#!/bin/bash
#
# Update all manuals to $1

cd ..
cg-update

cp SGC_ToneProt/README.txt SGC_ToneProt/SGC_ToneProt-README.txt
cp  HOWTO HOWTO.txt
scp COPYING HOWTO.txt README.txt sgc.glade doc/*.html SGC_ToneProt/SGC_ToneProt-README.txt ${1}

