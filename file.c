#include "sgc.h"
#include "file.h"
#include "signals-editor.h"

gchar *base = NULL;
gchar *file = NULL;

void clear() {
	gtk_list_store_clear(GTK_LIST_STORE(liststore));
	treevalid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(liststore), &mainIter);
	updateWidgets();
}

void setBase(gchar *newbase) {
	if (base != NULL)
		g_free(base);
	base = newbase;
}

void setFile(gchar *newfile) {
	if (file != NULL)
		g_free(file);

	file = newfile;
}

void fileOpen(gchar *filename) {
	gchar **testList = NULL;
	gchar *contents;
	gsize length;

	clear();

	if (g_file_get_contents(filename, &contents, &length, NULL) != FALSE) {
		gchar *processed;
		int i = 0;

		processed = checkPinyin(contents);
		testList = g_strsplit(processed, "\n", MAXTESTS);
		g_free(processed);

		while (	testList[i] != NULL) {
			if (g_str_has_prefix (testList[i], "#")) {
				/* Voeg commentaar toe ofzo */
			} else {
				add_entry(testList[i]);
			}
			i++;
		}
	}

	if (testList == NULL) {
		testList = g_strsplit("xu1\nci2", "\n", MAXTESTS);
		add_entry("xu1");
		add_entry("ci2");
	}

	g_strfreev(testList);
	gtk_window_set_title(GTK_WINDOW(glade_xml_get_widget(xml, "windowWordlist")), filename);

	setBase(g_path_get_dirname(filename));
	setFile(g_strdup(filename));

	treevalid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(liststore), &mainIter);
	updateWidgets();

}

void fileSave(gchar *filename) {
	if (treevalid == TRUE && filename != NULL) {
		GtkTreeIter  iter;
		gboolean     valid;

		/* Get first row in list store */
		valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(liststore), &iter);

		if (valid) {
			GString *contents = g_string_new("");
			if (contents != NULL) {
				while (valid)
				{
					gchar *string;
					gtk_tree_model_get(GTK_TREE_MODEL(liststore), &iter, COL_TEXT, &string, -1);
					contents = g_string_append(contents, string);
					contents = g_string_append(contents, "\r\n");

					valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(liststore), &iter);
				}
				{

					g_file_set_contents(filename, contents->str, -1, NULL);

					g_string_free(contents, true);
				}
			}
		}
	}
}
