#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <unistd.h> /* F_OK ?? */
#include <glib/gstdio.h>

#define LOCALEDIR "locales"
#define GLADESOURCE "singleword.glade"
#define SCRIPTPATH "SGC_ToneProt"
#define PITCHPATH  "PitchTiers"
#define RECORDPATH  "records"
#define RECORDTIME 3.5
#define CONFIGFILE "sgc.ini"
#define PRAATEXTERNAL 1

/* Which binary do we need to invoke in order to start a script */
#ifdef MINGW
#define PRAATBIN "praatcon.exe"
#elif PPC
#define PRAATBIN "./praat.ppc"
#else
#define PRAATBIN "./praat"
#endif


extern GladeXML *xml;

extern gdouble upperRegister;
extern gchar *active;
