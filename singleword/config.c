#include "singleword.h"

gdouble upperRegister = 200;

void configOpen() {
	GKeyFile *config = g_key_file_new();
	if (g_key_file_load_from_file(config, CONFIGFILE, G_KEY_FILE_NONE, NULL)) {
		upperRegister = g_key_file_get_double(config, "voice", "upperregister", NULL);

		if (upperRegister <= 0.0) upperRegister = 200.0;
	}
	g_key_file_free(config);
}
