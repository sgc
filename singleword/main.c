#include "singleword.h"
#include "praat.h"
#include "config.h"
#include "signals.h"

GladeXML *xml = NULL;

int main(int argc, char **argv)
{
        void * needed;
	needed = (void *)paint;
	needed = (void *)buttonCommit_clicked_cb;
	needed = (void *)buttonPlay_clicked_cb;
	needed = (void *)entryPinyin_key_release_event_cb;
	needed = (void *)buttonRecord_clicked_cb;
	needed = (void *)buttonHum_clicked_cb;

	gtk_init(&argc, &argv);

	bindtextdomain(g_get_application_name(), LOCALEDIR);

/*	textdomain(g_get_application_name());
 *
 *	This makes the application use a standard domain,
 *	instead of a changing program name. For example
 *	on win32 it will be sgc.exe
 */

	textdomain("singleword");

	xml = glade_xml_new(GLADESOURCE, NULL, g_get_application_name());

	if (xml == NULL) {
		g_error(_("Could not open the GUI!"));
		return -1;
	} else {
		configOpen();

		removeRecordings();

		if (!g_thread_supported()) {
			g_thread_init(NULL);
		}

		/* connect signal handlers */
		glade_xml_signal_autoconnect(xml);

	        g_thread_create((GThreadFunc)sound_init, NULL, FALSE, NULL);

		gtk_main();
		
		removeRecordings();
		return 0;
	}
}
