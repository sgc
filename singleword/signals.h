void entryPinyin_key_release_event_cb(GtkWidget *widget, GdkEventKey *event, gpointer user_data);
void buttonCommit_clicked_cb(GtkWidget *widget, gpointer user_data);
void buttonRecord_clicked_cb(GtkWidget *widget, gpointer user_data);
void buttonPlay_clicked_cb(GtkWidget *widget, gpointer user_data);
void buttonHum_clicked_cb(GtkWidget *widget, gpointer user_data);
void removeRecordings();
void paint (GtkWidget *widget, GdkEventExpose *eev, gpointer data);
void drawPitchTier(cairo_t *cr, gchar *filename, gint width, gint height, gdouble top);
