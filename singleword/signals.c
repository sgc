#include "singleword.h"
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <stdlib.h>
#include "praat.h"
#include "signals.h"
#include "cairo.h"

gchar *active = NULL;

void run_script(gchar *pinyin) {
	if (strlen(pinyin) > 1) {
		GString *cmd = g_string_sized_new (200);
		g_chdir(SCRIPTPATH);
#ifdef PRAATEXTERNAL
		g_string_printf(cmd, "%s DrawToneContour.praat %s %0.f", PRAATBIN, pinyin, upperRegister);
		system(cmd->str);
#else
		g_string_printf(cmd, "DrawToneContour.praat %s %0.f", pinyin, upperRegister);
		praat_executeScriptFromFileNameWithArguments(cmd->str);
		g_debug(cmd->str);
		Melder_flushError(NULL);
//		Melder_clearError();

#endif
		g_chdir("..");
		g_string_free(cmd, TRUE);
	}
}

static void commit(GtkWidget *widget) {
	if (active != NULL) g_free(active);
	active = g_strdup(gtk_entry_get_text(GTK_ENTRY(widget)));

	g_ascii_strdown(active, -1);
	g_strcanon(active, "#012345abcdefghijklmnopqrstuvwxyz\n", '-');

	gtk_entry_set_text(GTK_ENTRY(widget), active);

	run_script(active);

	gtk_widget_queue_draw(glade_xml_get_widget(xml, "drawingareaPitch"));
}


void entryPinyin_key_release_event_cb(GtkWidget *widget, GdkEventKey *event, gpointer user_data) {
	if (event != NULL && event->keyval ==  GDK_Return) {
		commit(widget);
	}
}

void buttonCommit_clicked_cb(GtkWidget *widget, gpointer user_data) {
	commit(widget);
}

void buttonRecord_clicked_cb(GtkWidget *widget, gpointer user_data) {
	g_thread_create((GThreadFunc)record, NULL, FALSE, NULL);	
}

void buttonPlay_clicked_cb(GtkWidget *widget, gpointer user_data) {
	g_thread_create((GThreadFunc)play, NULL, FALSE, NULL);	
}

void buttonHum_clicked_cb(GtkWidget *widget, gpointer user_data) {
	if (active != NULL) {
		GString *filename = g_string_sized_new (200);
		g_string_printf(filename, "%s%s%s-%0.f.PitchTier", PITCHPATH, G_DIR_SEPARATOR_S, active, upperRegister);

		if (g_access(filename->str, F_OK) == -1) run_script(active);

		g_thread_create((GThreadFunc)playHum, NULL, FALSE, NULL);

		g_string_free(filename, TRUE);
	}
}

void paint (GtkWidget *widget, GdkEventExpose *eev, gpointer data) {
	if (active != NULL) {
		cairo_t *cr;
		GString *filename = g_string_sized_new (200);
		GString *recordedPT = g_string_sized_new (200);

		g_string_printf(filename, "%s%s%s-%0.f.PitchTier", PITCHPATH, G_DIR_SEPARATOR_S, active, upperRegister);
		g_string_printf(recordedPT, "%s%s%s.PitchTier", RECORDPATH, G_DIR_SEPARATOR_S, active);

		if (g_access(filename->str, F_OK) == -1) run_script(active);

		cr = gdk_cairo_create (widget->window);
		cairo_set_line_width (cr, 1);
		cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
		if (g_access(filename->str, F_OK) == 0) {
			cairo_move_to (cr, 5, 15);
			cairo_set_source_rgb (cr, 0,0,0);
			cairo_show_text (cr, _("Reference Pitch"));


			drawPitchTier(cr, filename->str,
					widget->allocation.width,
					widget->allocation.height, upperRegister + 100.0);
		}
		g_string_free(filename, TRUE);

		if (g_access(recordedPT->str, F_OK) == 0) {
			cairo_move_to (cr, 15, 30);
			cairo_set_source_rgb (cr, 1,0,0);
			cairo_show_text (cr, _("Your Pitch"));

			drawPitchTier(cr, recordedPT->str,
					widget->allocation.width,
					widget->allocation.height, upperRegister + 100.0);

		}
		g_string_free(recordedPT, TRUE);
		cairo_destroy (cr);


	}
}

void removeRecordings() {
        GDir *record;
        gchar *file = g_build_filename(SCRIPTPATH, "lastExample.wav", NULL);
        g_unlink(file);
        g_free(file);
        if ((record = g_dir_open(RECORDPATH, 0, NULL)) != NULL) {
                const gchar *name;
                while ((name = g_dir_read_name(record)) != NULL) {
                        if (g_str_has_suffix(name, ".wav") || g_str_has_suffix(name, ".Pitch") || g_str_has_suffix(name, ".PitchTier")) {
                                file = g_build_filename(RECORDPATH, name, NULL);
                                g_unlink(file);
                                g_free(file);
                        }
                }
                g_dir_close(record);
        }
}

