#include "singleword.h"

void drawPitchTier(cairo_t *cr, gchar *filename, gint width, gint height, gdouble top) {
	gchar *contents;
	gsize length;
	if (g_file_get_contents(filename, &contents, &length, NULL) != TRUE) {
	} else {
		gdouble scalex = 0, scaley = 0;
		gdouble fromx = 0;
		gdouble middlex = 0;
		gdouble endx = 0;
		gdouble fromy = 0;
		gdouble middley = 0;
		gsize i = 0;
		gsize j = 0;
		gsize m = 0;
		int pointloc = 0;
		while (i < length) {
			if (contents[i] == '\n') {
				gchar * this = g_strndup(&contents[m], i-m);
				switch ( j ) {
					case 0:
					case 1:
					case 2:
					case 3:
						fromx = g_strtod(this, NULL);
						j++;
						break;
					case 4:
						endx  = g_strtod(this, NULL);
						scalex = width / endx;
						scaley = height / top;
						cairo_move_to(cr, fromx*scalex, 0);
					case 5:
						j++;
						break;
					default:
						if (pointloc == 0) {
							middlex = g_strtod(this, NULL);

							pointloc = 1;
						} else {
							gdouble to;
							middley = g_strtod(this, NULL);
							pointloc = 0;
							to = height - (middley*scaley);

							if ((middlex - fromx) < 0.015 && fromy != -1 && middley != -1) {
								cairo_line_to(cr, middlex*scalex,  to);
							} else {
								cairo_move_to(cr, middlex*scalex,  to);
							}
							fromx = middlex;
							fromy = middley;
						}

				}
				g_free(this);
				m = i+1;
			}
			i++;
		}
		cairo_stroke (cr);
	}
}
