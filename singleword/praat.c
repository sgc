#include "singleword.h"

#include "melder.h"
#include "NUMmachar.h"
#include "gsl_errno.h"

#include "site.h"
#include "machine.h"
#include "Strings.h"
#include "Sound.h"
#include "Vector.h"
#include "Thing.h"
#include "Pitch_to_Sound.h"
#include "Data.h"

gboolean setButtonsTrue() {

	gchar *feedbackTXT;
	gchar *contents;
	gboolean setButton = TRUE;
	GtkWidget *buttonRecord = glade_xml_get_widget(xml, "buttonRecord");
	GtkWidget *buttonPlay = glade_xml_get_widget(xml, "buttonPlay");
	GtkWidget *buttonHum = glade_xml_get_widget(xml, "buttonHum");
	GtkWidget *drawingareaPitch = glade_xml_get_widget(xml, "drawingareaPitch");
	gchar *path = g_build_filename(RECORDPATH, "singleword.wav", NULL);
	if (g_access(path, F_OK) == -1) setButton = FALSE;
	g_free(path);
	gtk_widget_set_sensitive(buttonPlay, setButton);
	gtk_widget_set_sensitive(buttonRecord, TRUE);
	gtk_widget_set_sensitive(buttonHum, TRUE);


	gtk_widget_queue_draw(drawingareaPitch);

	feedbackTXT = g_build_filename(SCRIPTPATH, G_DIR_SEPARATOR_S, "feedback.txt", NULL);

	if (g_file_get_contents(feedbackTXT, &contents, NULL, NULL) == TRUE) {
		GtkWidget *label = glade_xml_get_widget(xml, "labelFeedback");
		gtk_label_set_text(GTK_LABEL(label), contents);
		g_unlink(feedbackTXT);
	}
	g_free(feedbackTXT);

	return FALSE;
}

gboolean setButtonsFalse() {
	GtkWidget *buttonRecord = glade_xml_get_widget(xml, "buttonRecord");
	GtkWidget *buttonPlay = glade_xml_get_widget(xml, "buttonPlay");
	GtkWidget *buttonHum = glade_xml_get_widget(xml, "buttonHum");

	gtk_widget_set_sensitive(buttonRecord, FALSE);
	gtk_widget_set_sensitive(buttonPlay, FALSE);
	gtk_widget_set_sensitive(buttonHum, FALSE);
	return FALSE;
}

gpointer sound_init(void *args)
{
	Sound melderSoundFromMic;
#ifndef PRAATEXTERNAL
	gchar *argv[2] = { "Praat", "niets" };
	praat_init("Praat", 2, argv);
	praat_uvafon_init();
#endif

//	Melder_setMaximumAsynchronicity (Melder_SYNCHRONOUS);

	melderSoundFromMic = Sound_recordFixedTime (1, 1.0, 0.5, 44100, 1.0);
	forget(melderSoundFromMic);
	Melder_clearError();
	g_idle_add(setButtonsTrue, NULL);
	g_thread_exit(NULL);
	return NULL;
}

static void playInPraat(gchar *path) {
	if (g_access(path, F_OK) == 0) {
		structMelderFile file;
		Sound melderSoundFromFile;
		Melder_pathToFile(Melder_peekUtf8ToWcs(path), & file);
		melderSoundFromFile = Sound_readFromSoundFile (& file);
		if (! melderSoundFromFile) {
			g_warning(_("Praat cannot open the file!"));
		} else {
			Sound_play(melderSoundFromFile, NULL, NULL);
			forget(melderSoundFromFile);
		}
	} else {
		g_warning(_("The file %s just doesn't exist!"), path);
	}
	Melder_clearError();
}

gpointer playHum(void *args) {
	if (active != NULL) {
		GString *filename = g_string_sized_new (200);
		g_string_printf(filename, "%s%s%s-%0.f.wav", PITCHPATH, G_DIR_SEPARATOR_S, active, upperRegister);
		g_idle_add(setButtonsFalse, NULL);
		playInPraat(filename->str);
		g_string_free(filename, TRUE);
		g_idle_add(setButtonsTrue, NULL);
	}
	g_thread_exit(NULL);
	return NULL;
}


gpointer play(void *args) {
	gchar *path = g_build_filename(RECORDPATH, "singleword.wav", NULL);
	g_idle_add(setButtonsFalse, NULL);
	playInPraat(path);
	g_free(path);
	g_idle_add(setButtonsTrue, NULL);
	g_thread_exit(NULL);
	return NULL;
}

gpointer record(void *args) {
	if (active != NULL) {
		g_idle_add(setButtonsFalse, NULL);
		if (g_access(RECORDPATH, F_OK) == 0) {
			Sound melderSoundFromMic = Sound_recordFixedTime (1, 1.0, 0.5, 44100, RECORDTIME);
			if (! melderSoundFromMic) {
				g_warning(_("No sound from praat!"));
			} else {
				GString *cmd = g_string_sized_new(128);
				gchar *path;
				structMelderFile file;

				path = g_build_filename(RECORDPATH, "singleword.wav", NULL);


				Vector_scale(melderSoundFromMic, 0.99);
				Melder_pathToFile(Melder_peekUtf8ToWcs(path), &file);
				Sound_writeToAudioFile16 (melderSoundFromMic, &file, Melder_WAV);
				forget(melderSoundFromMic);

				g_chdir(SCRIPTPATH);
#ifdef PRAATEXTERNAL
				g_string_printf(cmd, "%s SGC_ToneProt.praat ..%s%s %s %0.f 3 none 0", PRAATBIN, G_DIR_SEPARATOR_S, path, active, upperRegister);
				system(cmd->str);
#else
				g_string_printf(cmd, "SGC_ToneProt.praat ..%s%s %s %0.f 3 none 0", G_DIR_SEPARATOR_S, path, active, upperRegister);
				praat_executeScriptFromFileNameWithArguments(cmd->str);
				Melder_clearError();
#endif

				g_free(path);
				g_chdir("..");
				g_string_free(cmd, TRUE);
			}
		} else {
			g_warning(_("Missing recording directory %s."), RECORDPATH);
		}
		Melder_clearError();
		g_idle_add(setButtonsTrue, NULL);
	}
	g_thread_exit(NULL);
	return NULL;
}

