#ifndef PITCHTIER

#include "sgc.h"
#include "frompraat.h"

/* Removed Speckle */
void Sampled_drawInside_cairo (I, cairo_t *cr, double xmin, double xmax, double ymin, double ymax, long ilevel, int unit)
{
        iam (Sampled);
        long ixmin, ixmax, ix, startOfDefinedStretch = -1;
        double *xarray = NULL, *yarray = NULL;
        double previousValue = NUMundefined;
        
	Function_unidirectionalAutowindow (me, & xmin, & xmax);
        Sampled_getWindowSamples (me, xmin, xmax, & ixmin, & ixmax);
        if (ClassFunction_isUnitLogarithmic (my methods, ilevel, unit)) {
                ymin = ClassFunction_convertStandardToSpecialUnit (my methods, ymin, ilevel, unit);
                ymax = ClassFunction_convertStandardToSpecialUnit (my methods, ymax, ilevel, unit);
        }
        if (ymax <= ymin) return;
        xarray = NUMdvector (ixmin - 1, ixmax + 1); cherror
        yarray = NUMdvector (ixmin - 1, ixmax + 1); cherror
        previousValue = Sampled_getValueAtSample (me, ixmin - 1, ilevel, unit);
        if (NUMdefined (previousValue)) {
                startOfDefinedStretch = ixmin - 1;
                xarray [ixmin - 1] = Sampled_indexToX (me, ixmin - 1);
                yarray [ixmin - 1] = previousValue;
        }
        for (ix = ixmin; ix <= ixmax; ix ++) {
                double x = Sampled_indexToX (me, ix), value = Sampled_getValueAtSample (me, ix, ilevel, unit);
                if (NUMdefined (value)) {
                        if (NUMdefined (previousValue)) {
                                xarray [ix] = x;
                                yarray [ix] = value;
                        } else {
                                startOfDefinedStretch = ix - 1;
                                xarray [ix - 1] = x - 0.5 * my dx;
                                yarray [ix - 1] = value;
                                xarray [ix] = x;
                                yarray [ix] = value;
                        }
                } else if (NUMdefined (previousValue)) {
                        Melder_assert (startOfDefinedStretch >= ixmin - 1);
                        if (ix > ixmin) {
                                xarray [ix] = x - 0.5 * my dx;
                                yarray [ix] = previousValue;
                                if (xarray [startOfDefinedStretch] < xmin) {
                                        double phase = (xmin - xarray [startOfDefinedStretch]) / my dx;
                                        xarray [startOfDefinedStretch] = xmin;
                                        yarray [startOfDefinedStretch] = phase * yarray [startOfDefinedStretch + 1] + (1.0 - phase) * yarray [startOfDefinedStretch];
                                }
                                Graphics_polyline_cairo (cr, ix + 1 - startOfDefinedStretch, & xarray [startOfDefinedStretch], & yarray [startOfDefinedStretch]);
                        }
                        startOfDefinedStretch = -1;
                }
                previousValue = value;
        }
        if (startOfDefinedStretch > -1) {
                double x = Sampled_indexToX (me, ixmax + 1), value = Sampled_getValueAtSample (me, ixmax + 1, ilevel, unit);
                Melder_assert (NUMdefined (previousValue));
                if (NUMdefined (value)) {
                        xarray [ixmax + 1] = x;
                        yarray [ixmax + 1] = value;
                } else {
                        xarray [ixmax + 1] = x - 0.5 * my dx;
                        yarray [ixmax + 1] = previousValue;
                }
                if (xarray [startOfDefinedStretch] < xmin) {
                        double phase = (xmin - xarray [startOfDefinedStretch]) / my dx;
                        xarray [startOfDefinedStretch] = xmin;
                        yarray [startOfDefinedStretch] = phase * yarray [startOfDefinedStretch + 1] + (1.0 - phase) * yarray [startOfDefinedStretch];
                }
                if (xarray [ixmax + 1] > xmax) {
                        double phase = (xarray [ixmax + 1] - xmax) / my dx;
                        xarray [ixmax + 1] = xmax;
                        yarray [ixmax + 1] = phase * yarray [ixmax] + (1.0 - phase) * yarray [ixmax + 1];
                }
                Graphics_polyline_cairo (cr, ixmax + 2 - startOfDefinedStretch, & xarray [startOfDefinedStretch], & yarray [startOfDefinedStretch]);
        }
end:
        NUMdvector_free (xarray, ixmin - 1);
        NUMdvector_free (yarray, ixmin - 1);
        Melder_clearError ();
}

void Graphics_polyline_cairo (cairo_t *cr, long numberOfPoints, double *xWC, double *yWC) {     /* Base 0. */
        short *xyDC;
        long i;
        if (! numberOfPoints) return;
        xyDC = Melder_malloc (short, 2 * numberOfPoints);
        if (! xyDC) return;
        for (i = 0; i < numberOfPoints; i ++) {
                xyDC [i + i] = xWC [i];
                xyDC [i + i + 1] = yWC [i];
        }
        polyline_cairo (cr, numberOfPoints, xyDC);
        Melder_free (xyDC);
}

void polyline_cairo (cairo_t *cr, long numberOfPoints, short *xyDC) {
	long i;
/*	int halfLine = 1;
	cairo_move_to (cr, xyDC [0] - halfLine, xyDC [1] - halfLine);
	for (i = 1; i < numberOfPoints; i ++) {
		cairo_line_to (cr, xyDC [i + i] - halfLine, xyDC [i + i + 1] - halfLine);
	}
	cairo_stroke(cr);*/


        cairo_new_path (cr);
        cairo_move_to (cr, (double) xyDC [0], (double) xyDC [1]);
        for (i = 1; i < numberOfPoints; i ++)
               cairo_line_to (cr, (double) xyDC [i + i], (double) xyDC [i + i + 1]);
        cairo_close_path (cr);
        cairo_stroke (cr);
}

#endif
