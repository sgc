#include "sgc.h"
#include "praat.h"
#include "signals.h"

#include <unistd.h> /* F_OK ?? */

#include "melder.h"
#include "NUMmachar.h"
#include "gsl_errno.h"

#include "site.h"
#include "machine.h"
#include "Strings.h"
#include "Sound.h"
#include "Vector.h"
#include "Thing.h"
#include "Pitch_to_Sound.h"
#include "Data.h"


gpointer sound_init(void *args)
{
	Sound melderSoundFromMic;

#ifndef PRAATEXTERNAL
	gchar *argv[2] = { "Praat", "niets" };
        praat_init("Praat", 2, argv);
        praat_uvafon_init();
#endif

	NUMmachar ();
	gsl_set_error_handler_off ();
//	Melder_setMaximumAsynchronicity (Melder_SYNCHRONOUS);

	melderSoundFromMic = Sound_recordFixedTime (1, 1.0, 0.5, 44100, 1.0);
	forget(melderSoundFromMic);
//	Melder_clearError();
	Melder_flushError (NULL);
	g_thread_exit(NULL);
	return NULL;
}

static void playInPraat(gchar *path) {
	if (g_access(path, F_OK) == 0) {
		structMelderFile file;
		Sound melderSoundFromFile;
		Melder_pathToFile(Melder_peekUtf8ToWcs(path), & file);
		melderSoundFromFile = Sound_readFromSoundFile (& file);
		if (! melderSoundFromFile) {
			g_warning(_("Praat cannot open the file!")); 
		} else {
			Sound_play(melderSoundFromFile, NULL, NULL);
			forget(melderSoundFromFile);
		}
	} else {
		g_warning(_("The file %s just doesn't exist!"), path);
	}
	Melder_clearError();
//	Melder_flushError (NULL);
}


static void playVoice() {
	if (treevalid) {
		gchar *string;
		gchar *path;
		gtk_tree_model_get(GTK_TREE_MODEL(liststore), &mainIter, COL_TEXT, &string, -1);
		path = voicepath(string);

		playInPraat(path);
		g_free(path);
		g_free(string);
	}
}

static void playHum() {
	if (treevalid) {
		gchar *string;
		gtk_tree_model_get(GTK_TREE_MODEL(liststore), &mainIter, COL_TEXT, &string, -1);
		gchar *path;
		gchar *upper = g_malloc(4);
		g_ascii_dtostr(upper, 4, upperRegister);
#ifdef WINCOMPAT
		path = g_strjoin(NULL, PITCHPATH, G_DIR_SEPARATOR_S, string, "-", upper, ".wav", NULL);
#else
		path = g_strjoin(NULL, PITCHPATH, G_DIR_SEPARATOR_S, string, "-", upper, ".Pitch", NULL);
#endif
		g_free(upper);

#ifdef WINCOMPAT
		playInPraat(path);
#else
		if (g_access(path, F_OK) == 0) {
			structMelderFile file;
			Pitch melderPitchFromFile;
#ifdef MINGW
			__declspec(dllimport) Pitch_Table classPitch;
#endif
			Thing_recognizeClassesByName (classPitch, NULL);
			Melder_pathToFile(Melder_peekUtf8ToWcs(path), & file);
			melderPitchFromFile = Data_readFromTextFile (& file);
			if (melderPitchFromFile != NULL) {
				Pitch_hum(melderPitchFromFile, 0, 0);
				forget(melderPitchFromFile);
			} else {
//				g_debug("PITCH IS NULL!");
			}
		}
#endif
		g_free(path);
		g_free(string);
		Melder_clearError();
	}
}


static void playBefore() {
	GtkWidget *gimiVoice = glade_xml_get_widget(xml, "checkmenuitemBeforeVoice");
	GtkWidget *gimiHum = glade_xml_get_widget(xml, "checkmenuitemBeforeHum");

	/* Check play Example */
	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(gimiVoice))) {
		/* Play Voice */
		playVoice();
	}

	/* Play Hum */
	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(gimiHum))) {
		playHum();
	}
}

gpointer example(void *args) {
	GtkWidget *gimiVoice = glade_xml_get_widget(xml, "checkmenuitemExampleVoice");
	GtkWidget *gimiHum = glade_xml_get_widget(xml, "checkmenuitemExampleHum");

	g_idle_add(setButtonsFalse, NULL);

	/* Check play Example */
	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(gimiVoice))) {
		/* Play Voice */
		playVoice();
	}

	/* Play Hum */
	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(gimiHum))) {
		playHum();
	}

	g_idle_add(setButtonsTrue, NULL);
	g_thread_exit(NULL);
	return NULL;
}

gpointer play(void *args) {
	if (treevalid) {
		gchar *path = pathToPlay();
		g_idle_add(setButtonsFalse, NULL);
		if (path != NULL) {
			playInPraat(path);
			g_free(path);
		}
		g_idle_add(setButtonsTrue, NULL);
	}
	g_thread_exit(NULL);
	return NULL;
}

gpointer record(void *args) {
	if (treevalid) {
		gchar *string;
		gtk_tree_model_get(GTK_TREE_MODEL(liststore), &mainIter, COL_TEXT, &string, -1);
//		g_debug("%s %d welcome!\n\n\n", __func__, rand());
		g_idle_add(setButtonsFalse, NULL);
		playBefore();
		if (g_access(RECORDPATH, F_OK) == 0) {
			Sound melderSoundFromMic = Sound_recordFixedTime (1, 1.0, 0.5, 44100, RECORDTIME);

			if (! melderSoundFromMic) {
				g_warning(_("No sound from praat!"));
			} else {
				GString *filename = g_string_sized_new(128);
				GString *cmd = g_string_sized_new(128);
				gchar *path;
				structMelderFile file;

				g_string_printf(filename, "%s.wav", string);

				path = g_build_filename(RECORDPATH, filename->str, NULL);
				g_string_free(filename, TRUE);

				Vector_scale(melderSoundFromMic, 0.99);
				Melder_pathToFile(Melder_peekUtf8ToWcs(path), &file);
				Sound_writeToAudioFile16 (melderSoundFromMic, &file, Melder_WAV);
				forget(melderSoundFromMic);

				g_chdir(SCRIPTPATH);
#ifdef PRAATEXTERNAL

				g_string_printf(cmd, "%s SGC_ToneProt.praat ..%s%s %s %0.f 3 none 0", PRAATBIN, G_DIR_SEPARATOR_S, path, string, upperRegister);
//				g_debug(cmd->str);
//				g_debug("EXECUTES! ");
				system(cmd->str);
//				g_debug("DONE!\n");

#else
				g_string_printf(cmd, "SGC_ToneProt.praat ..%s%s %s %0.f 3 none 0", G_DIR_SEPARATOR_S, path, string, upperRegister);
                                praat_executeScriptFromFileNameWithArguments(cmd->str);
                                Melder_clearError();
#endif
				g_free(path);

				g_chdir("..");
				g_string_free(cmd, TRUE);
			}
		} else {
			g_warning(_("Missing recording directory %s."), RECORDPATH);
		}
		g_free(string);
		Melder_clearError();
//		Melder_flushError (NULL);
		g_idle_add(setButtonsTrue, NULL);
//		g_debug("%s bye!\n", __func__);
	}
	g_thread_exit(NULL);
	return NULL;
}



