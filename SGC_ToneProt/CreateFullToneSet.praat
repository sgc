#!praat
#
# Create a full set of ba?ba? and ba? examples
# and write the sound files to file.
# You can specify the reiterated syllable and the 
# reference pitch
#
form Give syllable and F0
   word monosyllable ba1
   positive f0 300
endform

execute ToneScript.praat 'monosyllable$''monosyllable$' 'f0' 1 1 Sound
select Table ToneList
numExamples = Get number of rows
for i from 1 to numExamples
    select Table ToneList
    filename$ = Get value... 'i' Word
    if filename$ <> "------EMPTY"
    	select Sound 'filename$'
    	Write to WAV file... 'filename$'.wav
    	Remove
    endif
endfor
select Table ToneList
Remove

execute ToneScript.praat 'monosyllable$' 'f0' 1 1 Sound
select Table ToneList
numExamples = Get number of rows
for i from 1 to numExamples
    select Table ToneList
    filename$ = Get value... 'i' Word
    if filename$ <> "------EMPTY"
        select Sound 'filename$'
        Write to WAV file... 'filename$'.wav
        Remove
    endif
endfor
select Table ToneList
Remove
