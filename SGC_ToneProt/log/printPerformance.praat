#!praat
#
# Reads logFile.txt and counts the errors:
# Wrong, High, Narrow, and any of these
# Prints out the counts, total and error rate
#
# Splits out the results for today, if present.

Read Strings from raw text file... ./logFile.txt
Rename... logFile
istring = Get number of strings

wrong = 0
high = 0
narrow = 0
totalError = 0
total = 0

wrongToday = 0
highToday = 0
narrowToday = 0
totalErrorToday = 0
totalToday = 0

today$ = date$()
today$ = left$(today$, 10)+" [0-9 :]+ "+right$(today$, 4)

for i from 1 to istring
	select Strings logFile
	currentString$ = Get string... 'i'
	if not startsWith(currentString$, "\#")
		total += 1
		wrong += startsWith(currentString$, "Wrong")
		if rindex(currentString$, "High") > 0
			high += 1
		endif
		if rindex(currentString$, "Narrow") > 0
			narrow += 1
		endif
		if startsWith(currentString$, "Wrong") or rindex(currentString$, "High") > 0 or rindex(currentString$, "Narrow") > 0
			totalError += 1
		endif
		
		# Today
		if rindex_regex(currentString$, today$) > 0
			totalToday += 1
			wrongToday += startsWith(currentString$, "Wrong")
			if rindex(currentString$, "High") > 0
				highToday += 1
			endif
			if rindex(currentString$, "Narrow") > 0
				narrowToday += 1
			endif
			if startsWith(currentString$, "Wrong") or rindex(currentString$, "High") > 0 or rindex(currentString$, "Narrow") > 0
				totalErrorToday += 1
			endif
		endif
	endif
endfor
select Strings logFile
Remove


correct = total - totalError
rate = 0
if total > 0
	rate$ = fixed$((totalError/total)*100, 2)
endif
clearinfo

# Print results for today
if totalToday > 0
	printline Today 'today$'
	printline Wrong: 'wrong'
	printline High: 'high'
	printline Narrow: 'narrow'
	printline W+H+N: 'totalError'
	printline Responses: 'total'
	printline Error rate: 'rate$'%
	printline
endif

printline Total
printline Wrong: 'wrong'
printline High: 'high'
printline Narrow: 'narrow'
printline W+H+N: 'totalError'
printline Responses: 'total'
printline Error rate: 'rate$'%
printline
