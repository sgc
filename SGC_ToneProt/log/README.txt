Store recorded sounds

Collecting usage and perfomance data, and creating example audio

It is possible to record all utterances and write the recognition
results to a logFile.txt file. This option is not available from
the GUI. Currently, it is switched on by creating a file
in the SGC_ToneProt/log directory with the name logPerformance.txt 
(you can rename an existing stub file DoNotlogPerformance.txt to
logPerformance.txt). As long as there exists a file 
SGC_ToneProt/log/logPerformance.txt, every processed utterance
(audio and result) is stored in the directory SGC_ToneProt/log

