#! praat
#
# Calcualte the CoG and convert to Pitch object
# Silence the start (<-35 dB) and the end (<20 dB)

procedure CoGcalculation
    Copy... PCM

    select Sound PCM
    To TextGrid (silences)... 100 0 -35 0.1 0.1 silent sounding
    Rename... LowNoise
    select Sound PCM
    To TextGrid (silences)... 100 0 -20 0.1 0.1 silent sounding
    Rename... HighNoise

    select Sound PCM
    noprogress To Spectrogram... 0.025 8000 0.005 10 Gaussian
    select Spectrogram PCM
    To Matrix
    Rename... FE
    select Spectrogram PCM
    Remove
    select Matrix FE
    Copy... E

    select Matrix FE
    Formula... if(row>1) then self*y+self[row-1,col] else self*y fi
    To Sound (slice)... -1
    select Matrix FE
    Remove

    select Matrix E
    Formula... if(row>1) then self+self[row-1,col] else self fi
    To Sound (slice)... -1
    select Matrix E
    Remove

    select Sound FE
    Rename... CoG
    # Formula... 12*log2(self/Sound_E[col])
    Formula... (self/Sound_E[col])
    select Sound E
    Remove

    select Sound CoG
    Down to Matrix
    To Pitch
    
    # Remove silent parts: Initial Low Noise
    select TextGrid LowNoise
    numberOfSoundIntervals = Get number of intervals... 1
    for int from 1 to numberOfSoundIntervals
        select TextGrid LowNoise
        starttime = Get starting point... 1 'int'
        endtime = Get end point... 1 'int'
        soundValue$ = Get label of interval... 1 'int'
        if soundValue$ = "silent"
            select Pitch CoG
            Formula... if x >= 'starttime' and x < 'endtime' then 0 else self endif
        endif
    endfor
    
    # Remove silent parts: Final High Noise
    select Pitch CoG
    durationCoG = Get total duration
    endDurationCoG = durationCoG * 0.75
    select TextGrid HighNoise
    numberOfSoundIntervals = Get number of intervals... 1
    for int from 1 to numberOfSoundIntervals
        select TextGrid HighNoise
        starttime = Get starting point... 1 'int'
        endtime = Get end point... 1 'int'
        soundValue$ = Get label of interval... 1 'int'
        if soundValue$ = "silent" and endtime > endDurationCoG
            select Pitch CoG
            Formula... if x >= 'starttime' and x < 'endtime' then 0 else self endif
        endif
    endfor
    
    # Clean up
    select Sound CoG
    plus Sound PCM
    plus Matrix CoG
    plus TextGrid LowNoise
    plus TextGrid HighNoise
    Remove
    
    select Pitch CoG
endproc

