#! praat
#
# Synthesize Mandarin by concatenating syllables.
# Has some very rudimentary kludges for creating
# neutral tone syllables from fourth tone syllables 
# (should not sound acceptable)
#
# The sourcedir should be the mandaring sounds dir.

form type pinyin
    word pinyin ni3hao3
    word sourcedir ../wordlists/MandarinSounds
endform

pinyinProcessed$ = replace_regex$(pinyin$, "^\s*([a-zA-Z]+)3([a-zA-Z]+)3$", "\12\23", 0)

numSounds = 0
while pinyinProcessed$ > " "
	currentsyllable$ =  replace_regex$(pinyinProcessed$, "^\s*([a-zA-Z]+[0-9]).*$", "\1", 0)
        pinyinProcessed$ = replace_regex$(pinyinProcessed$, "^\s*[a-zA-Z]+[0-9](.*)$", "\1", 0)

	if rindex_regex(currentsyllable$, "0\s*$") > 0
		newSyllable$ = replace_regex$(currentsyllable$, "^\s*([a-zA-Z]+)0$", "\14", 0)
		Read from file... 'sourcedir$'/'newSyllable$'.wav
		Rename... 'currentsyllable$'
		Lengthen (PSOLA)... 75 600 0.7
		Rename... Shortened
		select Sound 'currentsyllable$'
		Remove
		select Sound Shortened
		Multiply... 0.71
		To Manipulation... 0.01 75 600
		Extract pitch tier
		Rename... Shortened
		Shift frequencies... 0 1000 -6 semitones
		select Manipulation Shortened
		plus PitchTier Shortened
		Replace pitch tier
		select Manipulation Shortened
		Get resynthesis (PSOLA)
		Rename... 'currentsyllable$'

		# Clean up
		select Manipulation Shortened
		plus Sound Shortened
		plus PitchTier Shortened
		Remove

		select Sound 'currentsyllable$'
	else
		Read from file... 'sourcedir$'/'currentsyllable$'.wav
	endif
	Rename... Sound
	To TextGrid (silences)... 100 0 -20 0.1 0.1 silent sounding
	Rename... Grid
        numIntervals = Get number of intervals... 1
	startTime = -1
        endTime = -1
	for i from 1 to numIntervals
		select TextGrid Grid
		currentLabel$ = Get label of interval... 1 'i'
		if currentLabel$ = "sounding"
			if startTime <= -1
				startTime = Get starting point... 1 'i'
			endif
			endTime = Get end point... 1 'i'
		endif
	endfor
	if startTime > 0.050 and rindex_regex(currentsyllable$, "^[ktpgdbz]") > 0
	    startTime = startTime - 0.050
	endif
	select Sound Sound
	Extract part... 'startTime' 'endTime' Rectangular 1.0 0
	Rename... 'currentsyllable$'

	select Sound Sound
	plus TextGrid Grid
	Remove

	numSounds = numSounds + 1
	if numSounds <= 1
		select Sound 'currentsyllable$'
		Rename... 'pinyin$'
        else
		select Sound 'pinyin$'
		plus Sound 'currentsyllable$'
		Concatenate
		Rename... tmpSound
		select Sound 'pinyin$'
		plus Sound 'currentsyllable$'
		Remove
		select Sound tmpSound
		Rename... 'pinyin$'
	endif
endwhile

