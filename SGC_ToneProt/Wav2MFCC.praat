#! praat
#
# Bulk conversion of WAV to MFCC files
# Only converts files that do not yet exist in the target directory

form Convert sounds to MFCC
    word source_dir ../wordlists/MandarinSounds
    word target_dir ../wordlists/MandarinSounds/mfcc
    positive number_of_Coefficients 12
endform

sourceExt$ = "wav"
targetExt$ = "mfcc"

# clearinfo

Create Strings as file list... SourceList 'source_dir$'/*.'sourceExt$'
numberOfFiles = Get number of strings

#    printline 'source_dir$'/*.'sourceExt$' 'numberOfFiles' 

for i from 1 to numberOfFiles
    select Strings SourceList
    inFile$ = Get string... 'i'
    outputFile$ = replace_regex$(inFile$, sourceExt$+"$", targetExt$, 1)

    if not fileReadable("'target_dir$'/'outputFile$'")
        Read from file... 'source_dir$'/'inFile$'
        Rename... Source
        noprogress To MFCC... 'number_of_Coefficients' 0.015 0.001 100.0 100.0 0.0
        Write to short text file... 'target_dir$'/'outputFile$'

        # Clean up
        select Sound Source
        plus MFCC Source
        Remove
    endif
endfor
select Strings SourceList
Remove
