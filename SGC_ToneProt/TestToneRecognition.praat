#!praat
#
# TestToneRecognition reads all audio-files in a
# source directory and tries to recognize them.
#
# The filenames should be:
# <pinyin corr>_<transcription>_<height Hz>_...wav
#
form Where are the files
    sentence dir ../../test
    sentence files *.wav
endform

exampleChoice$ = "none"
logPerformance = 0
precision = 3

clearinfo

Create Strings as file list... fileList 'dir$'/'files$'

number_of_files = Get number of strings

for i from 1 to number_of_files
    select Strings fileList
    currentFile$ = Get string... 'i'
    
    pinyin$ = replace_regex$(currentFile$, "^([^_]+)_.*$", "\1", 0)
    result$ = replace_regex$(currentFile$, "^[^_]+_([^_]+)_.*$", "\1", 0)
    height$ = replace_regex$(currentFile$, "^[^_]+_[^_]+_([^_]+)_.*$", "\1", 0)
    if index_regex(height$, "^F") > 0
        height$ = "300"
    elsif index_regex(height$, "^M") > 0
        height$ = "200"
    endif
    f1 = 'height$'

    # Take the correct boundaries between genders
    if f1 > 378
	currentRegister = 450
    elsif f1 > 252
	currentRegister = 300
    elsif f1 > 168
	currentRegister = 200
    else
	currentRegister = 150
    endif

    execute SGC_ToneProt.praat "'dir$'/'currentFile$'" 'pinyin$' 'currentRegister' 'precision' 'exampleChoice$' 'logPerformance'

    result$ < lastResult.txt
    result$ = replace_regex$(result$, "[\n]", " ", 0)
    printline 'result$''tab$''currentFile$'

    # pause 'result$'
endfor
