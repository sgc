# The rules for constructing the tone contours.
#
# This procedure works because in Praat, there is no namespace
# separation. All variables behave as if they are global
# (which they are).
#
# toneSyllable is the tone number on the current syllable
# 1-4, 0=neutral, 6=Dutch (garbage) intonation
#
# These procedures set the following pareameters
# and use them to create a Pitch Tier:
#
# toneFactor:  Duration scale factor for current tone
# startPoint:  Start of the tone
# endPoint:    end of the tone
# lowestPoint: bottom of tone 3
# point: The time of the next pitch value in the contour
#        ONLY USE AS: point = point + <fraction> * voicedDuration
#
# The following values are given by the calling routine
# DO NOT ALTER THEM
# 
# toneSyllable: tone number on the current syllable
# nextTone: tone number of next syllable or -1
# prevTone: tone number of previous syllable or -1
# lastFrequency: end-frequency of the previous syllable
#
# topLine: the frequency of the first tone
# frequency_Range: Range of tone four (1 octave)
# voicedDuration: Duration of voiced part of syllable
# 

# Procedure to scale the duration of the current syllable
procedure toneDuration
	if toneSyllable = 0
		zeroToneFactor = 0.5
        if prevTone = 2
            zeroToneFactor = 0.8 * zeroToneFactor
        elsif prevTone = 3
            zeroToneFactor = 1.1 * zeroToneFactor
        elsif prevTone = 4
            zeroToneFactor = 0.8 * zeroToneFactor
        endif
        toneFactor = zeroToneFactor * toneFactor
	elsif toneSyllable = 2
		toneFactor = 0.8
	elsif toneSyllable = 3
		toneFactor = 1.1
    elsif toneSyllable = 4
		toneFactor = 0.8
	endif
    
	# Next tone 0, then lengthen first syllable
	if nextTone = 0
		toneFactor = toneFactor * 1.2
	endif
endproc

# DO NOT CHANGE toneFactor BELOW THIS POINT

# Rules to create a tone
#
# Do not mess with the 'Add point...' commands
# unless you know what you are doing
# The 'point =' defines the time of the next pitch value
#
# start * ?Semit is a fall
# start / ?Semit is a rise
#
procedure toneRules
    #
    # Tone levels 1-5
    # Defined relative to the topline and the frequency range
    levelFive = topLine
    levelOne = topLine * frequency_Range
    levelThree = topLine * sqrt(frequency_Range)
    levelTwo = topLine * sqrt(sqrt(frequency_Range))
    levelFour = levelOne / sqrt(sqrt(frequency_Range))

    # First tone
	if toneSyllable = 1
        # Just a straight line
        startPoint = levelFive
        endPoint = levelFive
        
        # Two first tones, make them a little different
        if prevTone = 1
            startPoint = startPoint * 0.999
            endPoint = endPoint * 0.999
        endif
        
        # Write points
        Add point... 'point' 'startPoint'
        point = point + voicedDuration
        Add point... 'point' 'endPoint'
    # Second tone
	elsif toneSyllable = 2
        # Start halfway of the range - 1 semitone
        startPoint = levelThree * oneSemit
        # End 1 semitones above the first tone
        endPoint = levelFive / oneSemit
        # Special case: 2 followed by 1, stop short of the top-line
        # ie, 5 semitones above the start
        if nextTone = 1
            endPoint = startPoint / fiveSemit
	    endif
	    # Go lower if previous tone is 1
	    if prevTone = 1
	        startPoint = startPoint * oneSemit
        elsif prevTone = 4 or prevTone = 3
            # Special case: 2 following 4 or 3
            # Go 1 semitone up
	        startPoint = lastFrequency / oneSemit
            endPoint = levelFive
	    elsif prevTone = 2
        # Two consecutive tone 2, start 1 semitone higher
		    startPoint = startPoint / oneSemit
        endif
        # Define a midpoint at 1/3 of the duration
        midPoint = startPoint
             
        # Write points
        Add point... 'point' 'startPoint'
        # Next point a 1/3th of duration
        point = point + (voicedDuration)/3
        Add point... 'point' 'midPoint'
        # Remaining duration
        point = point + (voicedDuration)*2/3
        Add point... 'point' 'endPoint'
    # Third tone
	elsif toneSyllable = 3
        # Halfway the range
        startPoint = levelThree
        lowestPoint = levelOne * threeSemit
        # Protect pitch against "underflow"
        if lowestPoint < absoluteMinimum
            lowestPoint = absoluteMinimum
        endif
        # First syllable
        if nextTone < 0
            endPoint = startPoint
        # Anticipate rise in next tone
        elsif nextTone = 1 or nextTone = 4
            lowestPoint = levelOne / twoSemit
            endPoint = startPoint
        # Anticipate rise in next tone and stay low
        elsif nextTone = 2
            lowestPoint = levelOne / twoSemit
            endPoint = lowestPoint
        # Last one was low, don't go so much lower
        elsif prevTone = 4
            lowestPoint = levelOne * oneSemit
        # Anticipate rise in next tone and stay low
        elsif nextTone = 0
            lowestPoint = levelOne
            endPoint = lowestPoint / sixSemit
        else
            endPoint = startPoint
        endif
    
        # Write points
     	Add point... 'point' 'startPoint'
        # Go 1/3 of the duration down
	    point = point + (voicedDuration)*2/6
       	Add point... 'point' 'lowestPoint'
        # Go half the duration low
	    point = point + (voicedDuration)*3/6
       	Add point... 'point' 'lowestPoint'
        # Return in 1/6th of the duration
	    point = point + (voicedDuration)*1/6
       	Add point... 'point' 'endPoint'
    # Fourth tone
	elsif toneSyllable = 4
        # Start higher than tone 1 (by 2 semitones)
        startPoint = levelFive / twoSemit
        # Go down the full range
        endPoint = startPoint * frequency_Range
        
        
        # SPECIAL: Fall in following neutral tone
	    if nextTone = 0
	        endPoint = endPoint / threeSemit
        endif
     
        # Write points
        Add point... 'point' 'startPoint'
        # A plateau for 1/3th
        point = point + voicedDuration*1/3
        Add point... 'point' 'startPoint'
        # Go down the rest
        point = point + voicedDuration*2/3
        Add point... 'point' 'endPoint'
    # Neutral tone
	elsif toneSyllable = 0
        if lastFrequency > 0
            startPoint = lastFrequency
        else
            startPoint = levelThree / oneSemit
        endif

        if prevTone = 1
            startPoint = lastFrequency * twoSemit 
        elsif prevTone = 2
            startPoint = lastFrequency
        elsif prevTone = 3
            startPoint = lastFrequency / oneSemit
        elsif prevTone = 4
            startPoint = lastFrequency * oneSemit
        elsif lastFrequency > 0
            startPoint = lastFrequency * oneSemit
        endif

        # Catch all errors
        if startPoint <= 0
            startPoint = levelThree / oneSemit
        endif
            
       # Add spreading and some small or large de/inclination
        if prevTone = 1
        	midPoint = startPoint * frequency_Range / oneSemit
            endPoint = midPoint * oneSemit
        elsif prevTone = 2
            # 
        	midPoint = startPoint * fiveSemit
            endPoint = midPoint * twoSemit
        elsif prevTone = 3
        	midPoint = startPoint / twoSemit
            endPoint = midPoint
        elsif prevTone = 4
        	midPoint = startPoint * threeSemit
            endPoint = midPoint / oneSemit
        else
        	midPoint = startPoint * oneSemit
            endPoint = midPoint
        endif
                
        # Add a very short break to force
        if point = undefined
            point = 0
        endif
        
        Add point... 'point' 0
		point = point + 1/startPoint
        Add point... 'point' 0
		point = point + delta
        
        # Write points first 2/3 then decaying 1/3
        Add point... 'point' 'startPoint'
		point = point + (voicedDuration - 1/startPoint)*2/3
	    Add point... 'point' 'midPoint'
		point = point + (voicedDuration - 1/startPoint)*1/3
	    Add point... 'point' 'endPoint'
    # Dutch intonation
	else
        # Start halfway of the range
        startPoint = levelThree
        # Or continue from last Dutch "tone"
        if prevTone = 6
            startPoint = lastFrequency
        endif
        # Add declination
        endPoint = startPoint * oneSemit
        
        # Write points
        Add point... 'point' 'startPoint'
        point = point + voicedDuration
        Add point... 'point' 'endPoint'
	endif
endproc
