#! praat
#
#
# Draw the correct tone tracks
#
form Mandarin Tone contours
    word pinyin duo1shao3
    real register 300
endform

# Clean up input
if pinyin$ <> ""
    pinyin$ = replace_regex$(pinyin$, "^\s*(.+)\s*$", "\1", 1)
    pinyin$ = replace_regex$(pinyin$, "5", "0", 0)
endif

# Generate reference example
# Start with a range of 1 octave and a speed factor of 1
toneRange = 1.0
speedFactor = 1.0
execute ToneScript.praat 'pinyin$' 'register' 1 1 CorrectPitch

freqTop = 1.5 * register
Erase all
Select outer viewport... 0 6 0 4

# Draw Pitch track
select Pitch 'pinyin$'
Line width... 1
Green
Draw... 0 0 0 'freqTop' 1
Line width... 3
Green
Draw... 0 0 0 'freqTop' 0

24
Text top... no 'pinyin$'

Write to short text file... ../PitchTiers/'pinyin$'-'register'.Pitch

To Sound (hum)
Write to WAV file... ../PitchTiers/'pinyin$'-'register'.wav
Remove

select Pitch 'pinyin$'
Down to PitchTier
Write to short text file... ../PitchTiers/'pinyin$'-'register'.PitchTier
Remove

# Clean up
select Pitch 'pinyin$'
Remove


