#! praat
#
# Construct all tone patterns and look for the one closest to the given one
#

procedure FreeToneRecognition pinyin$ test_word$ exclude$ upperRegister freqRange durScale
    # Clean up input
    if pinyin$ <> ""
        pinyin$ = replace_regex$(pinyin$, "^\s*(.+)\s*$", "\1", 1)
        pinyin$ = replace_regex$(pinyin$, "5", "0", 0)
    endif

    referenceFrequency = 300
    frequencyFactor = referenceFrequency / upperRegister

    referenceExt$ = "pitch"
    
    # Bias Z-normalized value of the distance difference between smallest and correct
    biasDistance = 0.6
    
    # Debugging
    keepIntermediates = 0
    debug = 0

    # Generate reference tones
    execute ToneScript.praat 'pinyin$' 'upperRegister' 'freqRange' 'durScale' Pitch

    # Convert input to Pitch
    if test_word$ <> "" and test_word$ <> "REUSEPITCH"
        Read from file... 'test_word$'
        Rename... Source
    endif
    if test_word$ <> "REUSEPITCH"
        select Sound Source
        noprogress To Pitch (ac)... 0 60 15 yes 0.1 0.45 0.01 0.5 0.3 600
        # To Pitch... 0.0 60.0 600.0
        Formula... self*'frequencyFactor'; Normalize Pitch
        Rename... SourcePitch
    endif
    select Pitch SourcePitch

    countDistance = 0
    sumDistance = 0
    sumSqrDistance = 0
    correctDistance = -1

    smallestDistance=999999
    choiceReference$ = "empty"
    select Table ToneList
    listLength = Get number of rows
    for i from 1 to listLength
        select Table ToneList
        inFile$ = Get value... 'i' Word

        if (exclude$ = "" or rindex_regex(inFile$, exclude$) <= 0) and rindex_regex(inFile$, "[\d]") > 0 
            referenceName$ = inFile$
            select Pitch 'inFile$'
            plus Pitch SourcePitch
            noprogress To DTW... 24 10 yes yes no restriction
            Rename... DTW'inFile$'
            distance = Get distance (weighted)
            
            countDistance = countDistance + 1
            sumDistance = sumDistance + distance
            sumSqrDistance = sumSqrDistance + distance^2
            
            if pinyin$ = inFile$
                correctDistance = distance
            endif

            if debug > 0
                # printline 'distance' - 'inFile$'
            endif

            if distance < smallestDistance
                smallestDistance = distance
                choiceReference$ = "'inFile$'"
            endif

            # Clean up
            select DTW DTW'inFile$'

            if keepIntermediates = 0
                Remove
            endif
        endif
    endfor
    
    if countDistance > 1
        meanDistance = sumDistance / countDistance
        varDistance = (sumSqrDistance - sumDistance^2/countDistance)/(countDistance - 1)
        stdDistance = sqrt(varDistance)
        diffDistance = correctDistance - smallestDistance
        zDistance = diffDistance/stdDistance

        if debug > 0
            printline Match: 'pinyin$' <== 'choiceReference$' small='smallestDistance' Z='zDistance'
        endif

        if zDistance < biasDistance
            choiceReference$ = pinyin$
            smallestDistance = correctDistance
        endif
    endif
    

    # Clean up
    for i from 1 to listLength
        select Table ToneList
        inFile$ = Get value... 'i' Word

        if (exclude$ = "" or rindex_regex(inFile$, exclude$) <= 0) and rindex_regex(inFile$, "[\d]") > 0 

            # Clean up
            select Pitch 'inFile$'
            if keepIntermediates = 0
                Remove
            endif
        endif
    endfor

    select Table ToneList
    if test_word$ <> "" and test_word$ <> "REUSEPITCH"
        plus Sound Source
    endif
    if test_word$ <> "" and test_word$ <> "REUSEPITCH"
        plus Pitch SourcePitch
    endif
    if keepIntermediates = 0
        Remove
    endif

endproc
