# Test Initial Recognition

form Give input
	word test_dir ../../test
	word testreference_dir ../wordlists/CoGMandarinSounds
endform

clearinfo

Create Strings as file list... TestList 'test_dir$'/*.wav
numberOfTestReferences = Get number of strings

for j from 1 to numberOfTestReferences
    select Strings TestList
    testFile$ = Get string... 'j'

    pinyin$ = replace_regex$(testFile$, "^([a-z]+[0-9]+)[0-9a-zA-Z_\-\.]*$", "\1", 0)
    
    execute InitialRecognition.praat 'pinyin$' 'test_dir$'/'testFile$' 'testreference_dir$'

    result$ < lastInitialRecognitionResult.txt
    printline 'pinyin$': 'result$'
endfor

select Strings TestList
Remove
