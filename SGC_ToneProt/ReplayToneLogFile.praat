#! praat
#
# Script to replay the collection of examples in the
# logFile.txt as stored in the log/ directory
#
form Please indicate log file, log directory, and the default register
    sentence logfile logFile.txt
    sentence data_Source log
    real default_register 300
    positive precision_(st) 3
    boolean randomize 1
endform

clearinfo
exampleChoice$ = "Hum"
logPerformance = 0

Read Strings from raw text file... 'logfile$'
Rename... LogFile
numberofLines = Get number of strings
if randomize
	Randomize
endif
for i from 1 to numberofLines

    currentRegister =  default_register
    select Strings LogFile
    line$ = Get string... 'i'

    # Skip empty lines and comments
    if index_regex(line$, "^[ \t]*\#") <= 0 and index_regex(line$, "[a-zA-Z0-9]+") > 0   
        # Split line
        lengthLine = length(line$)
        numFields = 0
        currentStretch$ = line$
        currentPoint = 1
        while currentPoint <= lengthLine
            currentStretch$ = right$(line$, lengthLine - currentPoint + 1)
            nextSpace = index_regex(currentStretch$, "\t")
            if nextSpace <= 0
                numFields = numFields + 1
                field'numFields'$ = currentStretch$
                currentPoint = lengthLine + 1
            else
                numFields = numFields + 1
                field'numFields'$ = left$(currentStretch$, nextSpace-1)
                currentPoint = currentPoint + nextSpace
            endif
        endwhile

        # Get the relevant information (fields)
        pinyin$ = field2$
        currentSound$ = field'numFields'$
        currentRegister = extractNumber(field7$, "")

	# Play the current sound
	Open long sound file... 'data_Source$'/'currentSound$'
	totalDuration = Get total duration
	Play part... 0.0 'totalDuration'
	Remove

        # Start the recognizer
        execute SGC_ToneProt.praat "'data_Source$'/'currentSound$'" 'pinyin$' 'currentRegister' 'precision' 'exampleChoice$' 'logPerformance'

	result$ < lastResult.txt
	result$ = replace_regex$(result$, "[\n]", " ", 0)
	printline 'result$'

        pause Result - 'result$'
    endif
endfor

select Strings LogFile
Remove
