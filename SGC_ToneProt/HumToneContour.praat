#! praat
#
#
# Hum the correct tone tracks
#
form Mandarin Tone contours
    word pinyin duo1shao3
    real register 300
endform

# Clean up input
if pinyin$ <> ""
    pinyin$ = replace_regex$(pinyin$, "^\s*(.+)\s*$", "\1", 1)
    pinyin$ = replace_regex$(pinyin$, "5", "0", 0)
endif

# Generate reference example
# Start with a range of 1 octave and a speed factor of 1
toneRange = 1.0
speedFactor = 1.0
execute ToneScript.praat 'pinyin$' 'register' 1 1 CorrectPitch

# Hum Pitch track
select Pitch 'pinyin$'
Hum

# Clean up
select Pitch 'pinyin$'
Remove


