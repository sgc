#!praat
#
# Generate a Pitch object from the pinyin
#
# For testing the models.
#
form Enter pinyin and tone 1 frequency
	word inputWord xiang1gang3
	positive upperRegister_(Hz) 169
    real range_Factor 0.8408964152537146
    real durationScale 0.6511624373537163
    optionmenu generate 3
        option Pitch
        option Sound
        option CorrectPitch
        option CorrectSound
endform

# To supress the ToneList, change to 0
createToneList = 1
if rindex_regex(generate$, "Correct") > 0
	createToneList = 0
endif

# Limit lowest tone
absoluteMinimum = 80

# Clean up input
if inputWord$ <> ""
    inputWord$ = replace_regex$(inputWord$, "^\s*(.+)\s*$", "\1", 1)
endif

procedure extractTone syllable$
	toneSyllable = -1
	currentToneText$ = replace_regex$(syllable$, "^[^\d]+([\d]+)(.*)$", "\1", 0)
	toneSyllable = extractNumber(currentToneText$, "")
endproc

procedure convertVoicing voicingSyllable$
	# Remove tones
	voicingSyllable$ = replace_regex$(voicingSyllable$, "^([^\d]+)[\d]+", "\1", 0)
	# Convert voiced consonants
	voicingSyllable$ = replace_regex$(voicingSyllable$, "(ng|[wrlmny])", "C", 0)
	# Convert unvoiced consonants
	voicingSyllable$ = replace_regex$(voicingSyllable$, "(sh|ch|zh|[fsxhktpgqdbzcj])", "U", 0)
	# Convert vowels
	voicingSyllable$ = replace_regex$(voicingSyllable$, "([aiuoe�])", "V", 0)
endproc

# Add a tone movement. The current time point is 'point'
delta = 0.0000001
if durationScale <= 0
    durationScale = 1.0
endif
segmentDuration = 0.150
fixedDuration = 0.12
zeroToneFactor = 0.5

# 1/(12 semitones)
octave = 0.5
# 1/(9 semitones)
nineSemit = 0.594603557501361
# 1/(6 semitones)
sixSemit = 0.707106781186547
# 1/(3 semitones) down
threeSemit = 0.840896415253715
# 1/(2 semitones) down
twoSemit = 0.890898718140339
# 1/(1 semitones) down
oneSemit = 0.943874313
# 1/(5 semitones)
fiveSemit = threeSemit * twoSemit

frequency_Range = octave
if range_Factor > 0
    frequency_Range =  frequency_Range * range_Factor
endif

# Previous end frequency
lastFrequency = 0
procedure addToneMovement syllable$ topLine prevTone nextTone
	# Get tone
	toneSyllable = -1
	call extractTone 'syllable$'
    if toneSyllable = 3 and nextTone = 3
        toneSyllable = 2
    elsif syllable$ = "bu4" and nextTone = 4
        toneSyllable = 2
    endif

	# Get voicing pattern
	voicingSyllable$ = ""
	call convertVoicing 'syllable$'

	# Account for tones in duration
	toneFactor = 1
	if toneSyllable = 0
		toneFactor = 0.5
	elsif toneSyllable = 2
		toneFactor = 0.8
	elsif toneSyllable = 4
		toneFactor = 0.8
	endif
	toneFactor = toneFactor * durationScale

	# Unvoiced part
	if rindex_regex(voicingSyllable$, "U") = 1
		point = point + delta
        Add point... 'point' 0
		point = point + segmentDuration * toneFactor
        Add point... 'point' 0
	endif
	# Voiced part
	voiceLength$ = replace_regex$(voicingSyllable$, "U*([CV]+)U*", "\1", 0)
	voicedLength = length(voiceLength$)
	voicedDuration = toneFactor * (segmentDuration*voicedLength + fixedDuration)
	point = point + delta
    
    # Write contour of each tone
    # Note that tones are influenced by the previous (tone 0) and next (tone 3)
    # tones. Tone 6 is the Dutch intonation
    # sqrt(frequency_Range) is the mid point
    if topLine * frequency_Range < absoluteMinimum
        frequency_Range = absoluteMinimum / topLine
    endif
    
	if toneSyllable = 1
        Add point... 'point' 'topLine'
        point = point + voicedDuration
        Add point... 'point' 'topLine'
		lastFrequency = topLine
	elsif toneSyllable = 2
        # Halfway of the range
        startPoint = topLine * sqrt(frequency_Range)
        endPoint = topLine
        # Special case: 2 followed by 1, stop short of the top-line
        if nextTone = 1
            endPoint = startPoint / threeSemit
	    endif
	    # Go lower if previous tone is 1
	    if prevTone = 1
	        startPoint = startPoint * oneSemit
        elsif prevTone = 4
        # Special case: 2 following 4, stop short of top-line
            endPoint = startPoint / threeSemit
        endif

        Add point... 'point' 'startPoint'
        point = point + voicedDuration
        Add point... 'point' 'endPoint'
		lastFrequency = topLine
	elsif toneSyllable = 3
        # Halfway the range
        startPoint = topLine * sqrt(frequency_Range)
        lowestPoint = topLine * frequency_Range * threeSemit
        # Protect pitch against "underflow"
        if lowestPoint < absoluteMinimum
            lowestPoint = absoluteMinimum
        endif
        if nextTone <= 0
#            endPoint = topLine
            endPoint = startPoint
        elsif nextTone = 1 or nextTone = 4
            lowestPoint = topLine * frequency_Range / twoSemit
            endPoint = startPoint
        elsif nextTone = 2
            endPoint = topLine * frequency_Range / twoSemit
        else
            endPoint = startPoint
        endif

     	Add point... 'point' 'startPoint'
	    point = point + (voicedDuration)/3
       	Add point... 'point' 'lowestPoint'
	    point = point + (voicedDuration)/3
       	Add point... 'point' 'lowestPoint'
	    point = point + (voicedDuration)/3
       	Add point... 'point' 'endPoint'	
		lastFrequency = topLine
	elsif toneSyllable = 4
        startPoint = topLine / twoSemit
        endPoint = startPoint * frequency_Range
        Add point... 'point' 'startPoint'
        point = point + voicedDuration/3
        Add point... 'point' 'startPoint'
        point = point + voicedDuration*2/3
        Add point... 'point' 'endPoint'
		lastFrequency = endPoint
	elsif toneSyllable = 0
        if prevTone = 1 or prevTone = 2
			zeroPoint = topLine * sixSemit * oneSemit
        elsif prevTone = 3
            zeroPoint = topLine * threeSemit
        elsif prevTone = 4
			zeroPoint = topLine * frequency_Range / threeSemit
        else
 			zeroPoint = topLine * sixSemit * oneSemit     
        endif
        
        Add point... 'point' 'zeroPoint'
		point = point + voicedDuration
	    Add point... 'point' 'zeroPoint'
		lastFrequency = zeroPoint
	else
        dutchInton = topLine * sqrt(frequency_Range)
        if prevTone = 6
            dutchInton = lastFrequency
        endif
                
        endPoint = dutchInton * oneSemit
        Add point... 'point' 'dutchInton'
        point = point + voicedDuration
        Add point... 'point' 'endPoint'
		lastFrequency = endPoint
	endif

endproc

# Split input into syllables
margin = 0.25

procedure wordToTones wordInput$ highPitch
	currentRest$ = wordInput$;
	syllableCount = 0
	length = 2 * margin
    
    # Split syllables
	while rindex_regex(currentRest$, "^[^\d]+[\d]+") > 0
        syllableCount += 1
        syllable'syllableCount'$ = replace_regex$(currentRest$, "^([^\d]+[\d]+)(.*)$", "\1", 1)
		currentSyllable$ = syllable'syllableCount'$

		# Get the tone
		call extractTone 'currentSyllable$'
		toneSyllable'syllableCount' = toneSyllable
		currentTone = toneSyllable'syllableCount'

		# Get the Voicing pattern
		call convertVoicing 'currentSyllable$'
		voicingSyllable'syllableCount'$ = voicingSyllable$
		currentVoicing$ = voicingSyllable'syllableCount'$

		# Calculate new length
		toneFactor = 1
		if currentTone = 0
			toneFactor = 0.5
		elsif currentTone = 4
			toneFactor = 0.8
		endif
	    toneFactor = toneFactor * durationScale
 
		length = length + toneFactor * (length(voicingSyllable'syllableCount'$) * (segmentDuration + delta) + fixedDuration)

		# Next round
		currentRest$ = replace_regex$(currentRest$, "^([^\d]+[\d]+)(.*)$", "\2", 1)
   
		# Safety valve
		if syllableCount > 2000
   			exit
		endif
	endwhile

	# Create tone pattern
	Create PitchTier... 'wordInput$' 0 'length'

	# Add start margin
	lastFrequency = 0
	point = margin
	Add point... 'point' 0

    lastTone = -1
    followTone = -1
	for i from 1 to syllableCount
		currentSyllable$ = syllable'i'$
        currentTone = toneSyllable'i'
        followTone = -1
        if i < syllableCount
            j = i+1
            followTone = toneSyllable'j'
        endif
        
		call addToneMovement 'currentSyllable$' 'highPitch' 'lastTone' 'followTone'
        
        lastTone = currentTone
	endfor

	# Add end margin
	point = point + delta
	Add point... 'point' 0
	point = point + margin
	Add point... 'point' 0
endproc

procedure generateWord whatToGenerate$ theWord$ upperRegister
	call wordToTones 'theWord$' 'upperRegister'
	# Generate pitch
    select PitchTier 'theWord$'
    noprogress To Pitch... 0.0125 60.0 600.0
	Rename... theOrigWord
	Smooth... 10
	Rename... 'theWord$'
	select Pitch theOrigWord
	Remove

    # Generate sound if wanted
    select Pitch 'theWord$'
    if rindex_regex(whatToGenerate$, "Sound") > 0
	    noprogress To Sound (hum)
    endif

    # Clean up
    select PitchTier 'theWord$'
    if rindex_regex(whatToGenerate$, "Sound") > 0
        plus Pitch 'theWord$'
    endif
    Remove
endproc

# Horrible clutch to get an empty Strings list
if createToneList = 1
    Read Strings from raw text file... ToneScript.praat
    Rename... ToneList
    listLength = Get number of strings
    for i from 1 to listLength
        select Strings ToneList
        Set string... 'i' ------EMPTY
    endfor
endif

syllableCount = length(replace_regex$(inputWord$, "[^\d]+([\d]+)", "1", 0))
wordNumber = 0
if rindex(generate$, "Correct") <= 0
    for first from 1 to 6
	    currentWord$ = replace_regex$(inputWord$, "^([^\d]+)([\d]+)(.*)$", "\1'first'\3", 1)
	    for second from 0 to 6
		    if (first <> 5 and second <> 5) and (syllableCount > 1 or second == 1)
			    currentWord$ = replace_regex$(currentWord$, "^([^\d]+)([\d]+)([^\d]+)([\d]+)$", "\1'first'\3'second'", 1)
                # Write name in list
                wordNumber = wordNumber+1
                if createToneList = 1
                    select Strings ToneList
                    Set string... 'wordNumber' 'currentWord$'
                endif

                # Actually, generate something
                call generateWord 'generate$' 'currentWord$' 'upperRegister'
		    endif
	    endfor
    endfor
else
    call generateWord 'generate$' 'inputWord$' 'upperRegister'
endif
