#! praat
#
#     SpeakGoodChinese: SGC_ToneRecognizer.praat processes student utterances 
#     and generates a report on their tone production
#     
#     Copyright (C) 2007  R.J.J.H. van Son
#     The SpeakGoodChinese team are:
#     Guangqin Chen, Zhonyan Chen, Stefan de Koning, Eveline van Hagen, 
#     Rob van Son, Dennis Vierkant, David Weenink
# 
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 

form Mandarin Tone recognition
    word currentSound lastExample.wav
    word pinyin shi4
    real register 300
    positive precision_(st) 3
    optionmenu example 4
	option none
        option Pictures
        option Hum
        option Replay
    boolean logPerformance
endform

include ToneRecognition.praat

# Check if log/logPerfomance* exists
# If it does, do log the performance
Create Strings as file list... logList log/logPerformance*
number_of_logfiles = Get number of strings
if number_of_logfiles > 0
    logPerformance = 1
endif
# Remove object
select Strings logList
Remove

# Clean up input
if pinyin$ <> ""
    pinyin$ = replace_regex$(pinyin$, "^\s*(.+)\s*$", "\1", 1)
    pinyin$ = replace_regex$(pinyin$, "5", "0", 0)
endif

# Reduction (lower register and narrow range) means errors
# The oposite mostly not. Asymmetry alows more room upward
# than downward (asymmetry = 2 => highBoundaryFactor ^ 2)
asymmetry = 2

# Kill octave jumps: DANGEROUS
killOctaveJumps = 0
# Silence soft noise: DANGEROUS
silenceSoftNoises = 0

# Limit pitch range
minimumPitch = 60
maximumPitch = 500
if register > 400
    minimumPitch = 100
    maximumPitch = 600
elsif register > 250
    minimumPitch = 80
    maximumPitch = 500
else
    minimumPitch = 60
    maximumPitch = 400
endif


# Stick to the raw recognition results or not
ultraStrict = 0

currentTestWord$ = pinyin$
spacing = 0.5
precisionFactor = 2^(precision/12)
highBoundaryFactor = precisionFactor ^ asymmetry
lowBoundaryFactor = 1/precisionFactor

# Generate reference example
# Start with a range of 1 octave and a speed factor of 1
toneRange = 1.0
speedFactor = 1.0
upperRegisterInput = register
execute ToneScript.praat 'currentTestWord$' 'upperRegisterInput' 1 1 CorrectPitch
# Get range and top
select Pitch 'currentTestWord$'
durationModel = Get total duration
maximumModelFzero = Get quantile... 0 0 0.95 Hertz
minimumModelFzero = Get quantile... 0 0 0.05 Hertz
modelPitchRange = 2
if minimumModelFzero > 0
    modelPitchRange = maximumModelFzero / minimumModelFzero
endif

# Get the sounds
Read from file... 'currentSound$'
Rename... inputSound

# Get the sound part
maximumPower = -1
select Sound inputSound
soundlength = Get total duration
To TextGrid (silences)... 'minimumPitch' 0 -30 0.5 0.1 silent sounding
Rename... inputSound

select TextGrid inputSound
numberofIntervals = Get number of intervals... 1

# Remove buzzing and other obnoxious sounds (if switched on)
for i from 1 to numberofIntervals
   select TextGrid inputSound
   value$ = Get label of interval... 1 'i'
   begintime = Get starting point... 1 'i'
   endtime = Get end point... 1 'i'

   # Remove noise
   if value$ = "silent" and silenceSoftNoises > 0
	select Sound inputSound
	Set part to zero... 'begintime' 'endtime' at nearest zero crossing
   endif
endfor

# Select target speech
counter = 1
for i from 1 to numberofIntervals
   select TextGrid inputSound

   value$ = Get label of interval... 1 'i'
   begintime = Get starting point... 1 'i'
   endtime = Get end point... 1 'i'

   if value$ != "silent"
       if begintime > spacing / 2
           begintime = begintime - (spacing / 2)
       else
           begintime = 0
       endif
       if endtime + (spacing / 2) < soundlength
           endtime = endtime + (spacing / 2)
       else
           endtime = soundlength
       endif

       select Sound inputSound
       Extract part... 'begintime' 'endtime' Rectangular 1.0 no
       Rename... newSource
       Subtract mean
       newPower = Get power... 0 0
       if newPower > maximumPower
           if maximumPower > 0
               select Sound Source
               Remove
           endif
           select Sound newSource
           Rename... Source
           maximumPower = newPower
       else
           select Sound newSource
           Remove
       endif
       # 
   endif
endfor
select Sound inputSound
plus TextGrid inputSound
Remove

# Calculate pitch
select Sound Source
durationSource = Get total duration
# noprogress To Pitch (ac)... 0 'minimumPitch' 15 yes 0.2 0.6 0.02 0.5 0.3 'maximumPitch'
noprogress To Pitch... 0.0 'minimumPitch' 'maximumPitch'
Rename... SourcePitch

# It is rather dangerous to kill Octave errors, so be careful
if killOctaveJumps > 0
    Rename... OldSource
    Kill octave jumps
    Rename... SourcePitch
    select Pitch OldSource
    Remove
endif

# Remove all pitch points outside a band around the upper register
select Pitch SourcePitch
upperCutOff = 1.7*upperRegisterInput
lowerCutOff = upperRegisterInput/4
Formula... if self > 'upperCutOff' then -1 else self endif
Formula... if self < 'lowerCutOff' then -1 else self endif

# Get range and top
select Pitch SourcePitch
maximumRecFzero = Get quantile... 0 0 0.95 Hertz
timeMaximum = Get time of maximum... 0 0 Hertz Parabolic
minimumRecFzero = Get quantile... 0 0 0.05 Hertz
timeMinimum = Get time of minimum... 0 0 Hertz Parabolic
if maximumRecFzero = undefined
    # Read and select the feedbacktext
    Read Table from tab-separated file... feedback/ToneFeedback.txt
    Rename... ToneFeedback
    numberOfFeedbackRows = Get number of rows

    # Determine what should be told to the student
    recognitionText$ =  "'currentTestWord$': ???"
    for i from 1 to numberOfFeedbackRows
        select Table ToneFeedback
        toneOne$ = Get value... 'i' T1
        toneTwo$ = Get value... 'i' T2
        toneText$ = Get value... 'i' Feedback

        if toneOne$ = "NoSound"
            feedbackText$ = toneText$
        endif
    endfor
    recognitionText$ > feedback.txt
    newline$ >> feedback.txt
    feedbackText$ >> feedback.txt
    newline$ >> feedback.txt
    
    select Table ToneFeedback
    Remove
    exit Error, nothing recorded
endif
recPitchRange = 2
if minimumRecFzero > 0
   recPitchRange = maximumRecFzero / minimumRecFzero
endif
newUpperRegister = maximumRecFzero / maximumModelFzero * upperRegisterInput
newToneRange = recPitchRange / modelPitchRange

registerUsed$ = "OK"
rangeUsed$ = "OK"
if newUpperRegister > highBoundaryFactor * upperRegisterInput
   newUpperRegister = highBoundaryFactor * upperRegisterInput
   registerUsed$ = "High"
elsif newUpperRegister < lowBoundaryFactor * upperRegisterInput
   newUpperRegister = lowBoundaryFactor * upperRegisterInput
   registerUsed$ = "Low"
endif
if newToneRange > highBoundaryFactor
   newToneRange = highBoundaryFactor
   rangeUsed$ = "Wide"
elsif newToneRange < lowBoundaryFactor
   newToneRange = lowBoundaryFactor
   rangeUsed$ = "Narrow"
endif

# Duration 
if durationModel > spacing
   speedFactor = (durationSource - spacing) / (durationModel - spacing)
endif

# Round values
newUpperRegister = round(newUpperRegister)

# Remove all pitch points outside a band around the upper register
select Pitch SourcePitch
upperCutOff = 1.5*newUpperRegister
lowerCutOff = newUpperRegister/3
Formula... if self > 'upperCutOff' then -1 else self endif
Formula... if self < 'lowerCutOff' then -1 else self endif

if killOctaveJumps > 0
    Rename... OldSourcePitch
    Kill octave jumps
    Rename... SourcePitch
    select Pitch OldSourcePitch
    Remove
endif

# It is good to have the lowest and highest pitch frequencies
select Pitch SourcePitch
timeMaximum = Get time of maximum... 0 0 Hertz Parabolic
timeMinimum = Get time of minimum... 0 0 Hertz Parabolic

# Clean up the old example pitch
select Pitch 'currentTestWord$'
Remove

# Do the tone recognition
call FreeToneRecognition 'currentTestWord$' "REUSEPITCH" "" 'newUpperRegister' 'newToneRange' 'speedFactor'
#execute ToneScript.praat 'currentTestWord$' 'newUpperRegister' 'newToneRange' 'speedFactor' CorrectPitch
execute ToneScript.praat 'currentTestWord$' 'upperRegisterInput' 'newToneRange' 'speedFactor' CorrectPitch

# Special cases
originalRecognizedWord$ = choiceReference$
if ultraStrict = 0
    # First syllable: 2<->3 (6) exchanges (incl 6)
    if rindex_regex(currentTestWord$, "^[a-zA-Z]+2[a-zA-Z]+[0-4]$") > 0
        if rindex_regex(choiceReference$, "^[a-zA-Z]+[36][a-zA-Z]+[0-4]$") > 0
            choiceReference$ = replace_regex$(choiceReference$, "[36]([a-zA-Z]+[0-4])$", "2\1", 0)
        endif
    elsif rindex_regex(currentTestWord$, "^[a-zA-Z]+3[a-zA-Z]+[0-4]$") > 0
        if rindex_regex(choiceReference$, "^[a-zA-Z]+[26][a-zA-Z]+[0-4]$") > 0
            choiceReference$ = replace_regex$(choiceReference$, "[26]([a-zA-Z]+[0-4])$", "3\1", 0)
        endif
    # A single second tone is often misidentified as a neutral tone, 
    # A real neutral tone would be too low or too narrow and be discarded
    # Leaves us with erroneous tone 4
    elsif rindex_regex(currentTestWord$, "^[a-zA-Z]+2$") > 0
        if rindex_regex(choiceReference$, "^[a-zA-Z]+0$") > 0 and timeMinimum < timeMaximum
            choiceReference$ = replace_regex$(choiceReference$, "0", "2", 0)
        endif
    # A single fourth tone is often misidentified as a neutral tone, 
    # A real neutral tone would be too low or too narrow and be discarded
    # Leaves us with erroneous tones 2 and 3
    elsif rindex_regex(currentTestWord$, "^[a-zA-Z]+4$") > 0
        if rindex_regex(choiceReference$, "^[a-zA-Z]+0$") > 0 and timeMaximum < timeMinimum
            choiceReference$ = replace_regex$(choiceReference$, "0", "4", 0)
        endif
    endif

    # Second (last) syllable, 0<->6 exchanges and 2<->3
    # A recognized 0 after a 4 can be a 2: 4-0 => 4-2
    if rindex_regex(currentTestWord$, "[a-zA-Z]+[4][a-zA-Z]+2$") > 0
        if rindex_regex(choiceReference$, "[a-zA-Z]+[4][a-zA-Z]+[0]$") > 0
            choiceReference$ = replace_regex$(choiceReference$, "[0]$", "2", 0)
        endif
    endif
    # A final 6 after a valid tone is often a recognition error
    # A final 6 can be a 0
    if rindex_regex(currentTestWord$, "[a-zA-Z]+[0-9][a-zA-Z]+0$") > 0
        if rindex_regex(choiceReference$, "[a-zA-Z]+[0-4][a-zA-Z]+6$") > 0
            choiceReference$ = replace_regex$(choiceReference$, "6$", "0", 0)
        endif
    # Second (last) syllable, 2<->3 exchanges after [23] tones
    # A recognized 6 (or 3) after a valid tone [1-4] is mostly wrong, can be a 2
    elsif rindex_regex(currentTestWord$, "[a-zA-Z]+[1-4][a-zA-Z]+2$") > 0
        if rindex_regex(choiceReference$, "[a-zA-Z]+[1-4][a-zA-Z]+[36]$") > 0
            choiceReference$ = replace_regex$(choiceReference$, "[36]$", "2", 0)
        endif
    # A recognized 6 after a [23] is mostly wrong, can be a 3
    elsif rindex_regex(currentTestWord$, "[a-zA-Z]+[23][a-zA-Z]+3$") > 0
        if rindex_regex(choiceReference$, "[a-zA-Z]+[23][a-zA-Z]+[26]$") > 0
            choiceReference$ = replace_regex$(choiceReference$, "[26]$", "3", 0)
        endif
    # A recognized 6 after a [3] is mostly wrong, can be a 1
    elsif rindex_regex(currentTestWord$, "[a-zA-Z]+[3][a-zA-Z]+1$") > 0
        if rindex_regex(choiceReference$, "[a-zA-Z]+[3][a-zA-Z]+[6]$") > 0
            choiceReference$ = replace_regex$(choiceReference$, "[6]$", "1", 0)
        endif
    endif

    # Clean up odd things constructed with special cases
    # Target is 3-3, but recognized is 2-3, which is CORRECT. Change it into 3-3
    if rindex_regex(currentTestWord$, "[a-zA-Z]+[3][a-zA-Z]+[3]$") > 0
        if rindex_regex(choiceReference$, "[a-zA-Z]+[2][a-zA-Z]+[3]$") > 0
            choiceReference$ = replace_regex$(choiceReference$, "[2]([a-zA-Z]+[3])$", "3\1", 0)
        endif
    endif
endif

# If wrong, then undo all changes
if currentTestWord$ != choiceReference$
    choiceReference$ = originalRecognizedWord$
endif

###############################################
#
# Experimental recognition of initial sound
#
###############################################

toneChoiceReference$ = choiceReference$
if fileReadable("../wordlists/CoGMandarinSounds/'choiceReference$'.cog")
    select Sound Source
    Write to WAV file... lastExample.wav

    execute InitialRecognition.praat 'choiceReference$' lastExample.wav ../wordlists/CoGMandarinSounds
    choiceReference$ < lastInitialRecognitionResult.txt
endif

###############################################
#
# Report
#
###############################################
result$ = "'tab$''currentTestWord$''tab$''choiceReference$''tab$''newUpperRegister''tab$''newToneRange''tab$''speedFactor''tab$''registerUsed$''tab$''rangeUsed$'"
if currentTestWord$ = toneChoiceReference$
   result$ = "Correct:"+result$
else
   result$ = "Wrong:"+result$
endif

# Initialize result texts
recognitionText$ =  "'currentTestWord$': "
choiceText$ = replace_regex$(choiceReference$, "6", "\?", 0)
feedbackText$ = "----"

# Read and select the feedbacktext
Read Table from tab-separated file... feedback/ToneFeedback.txt
Rename... ToneFeedback
numberOfFeedbackRows = Get number of rows

# Separate tone from pronunciation errors
currentToneWord$ = replace_regex$(currentTestWord$, "[a-z]+", "\*", 0)
choiceToneReference$ = replace_regex$(choiceReference$, "[a-z]+", "\*", 0)

# Determine what should be told to the student
if registerUsed$ = "Low"
    recognitionText$ = recognitionText$ + "???"
    for i from 1 to numberOfFeedbackRows
        select Table ToneFeedback
        toneOne$ = Get value... 'i' T1
        toneTwo$ = Get value... 'i' T2
        toneText$ = Get value... 'i' Feedback

        if toneOne$ = "Low"
            feedbackText$ = toneText$
        endif
    endfor
elsif rangeUsed$ = "Narrow"
    recognitionText$ = recognitionText$ + "???"
    for i from 1 to numberOfFeedbackRows
        select Table ToneFeedback
        toneOne$ = Get value... 'i' T1
        toneTwo$ = Get value... 'i' T2
        toneText$ = Get value... 'i' Feedback

        if toneOne$ = "Narrow"
            feedbackText$ = toneText$
        endif
    endfor
elsif registerUsed$ = "High"
    recognitionText$ = recognitionText$ + choiceText$
    for i from 1 to numberOfFeedbackRows
        select Table ToneFeedback
        toneOne$ = Get value... 'i' T1
        toneTwo$ = Get value... 'i' T2
        toneText$ = Get value... 'i' Feedback

        if toneOne$ = "High"
            feedbackText$ = toneText$
        endif
    endfor
elsif rangeUsed$ = "Wide"
    recognitionText$ = recognitionText$ + choiceText$
    for i from 1 to numberOfFeedbackRows
        select Table ToneFeedback
        toneOne$ = Get value... 'i' T1
        toneTwo$ = Get value... 'i' T2
        toneText$ = Get value... 'i' Feedback

        if toneOne$ = "Wide"
            feedbackText$ = toneText$
        endif
    endfor
# Bad tones, first handle first syllable
elsif rindex_regex(choiceReference$, "^[a-zA-Z]+6") > 0
    recognitionText$ = recognitionText$ + choiceText$
    # First syllable
    for i from 1 to numberOfFeedbackRows
        select Table ToneFeedback
        toneOne$ = Get value... 'i' T1
        toneTwo$ = Get value... 'i' T2
        toneText$ = Get value... 'i' Feedback

        # 
        feedbackText$ = ""
        if toneOne$ = "6"
            recognitionText$ = recognitionText$ + " ('toneText$')"
        elsif rindex_regex(currentTestWord$, "^[a-zA-Z]+'toneOne$'") > 0 and toneTwo$ = "-"
            feedbackText$ = feedbackText$ + toneText$ + " "
        endif
    endfor
# Bad tones, then handle second syllable
elsif rindex_regex(choiceReference$, "[a-zA-Z]+6$") > 0
    recognitionText$ = recognitionText$ + choiceText$
    # Last syllable
    for i from 1 to numberOfFeedbackRows
        select Table ToneFeedback
        toneOne$ = Get value... 'i' T1
        toneTwo$ = Get value... 'i' T2
        toneText$ = Get value... 'i' Feedback

        # 
        feedbackText$ = ""
        if toneOne$ = "6"
            recognitionText$ = recognitionText$ + " ('toneText$')"
        elsif rindex_regex(currentTestWord$, "[a-zA-Z]+'toneOne$'$") > 0 and toneTwo$ = "-"
            feedbackText$ = feedbackText$ + toneText$ + " "
        endif
    endfor
# Just plain wrong tones
elsif currentToneWord$ <> choiceToneReference$
    recognitionText$ = recognitionText$ + choiceText$
    for i from 1 to numberOfFeedbackRows
        select Table ToneFeedback
        toneOne$ = Get value... 'i' T1
        toneTwo$ = Get value... 'i' T2
        toneText$ = Get value... 'i' Feedback

        if rindex_regex(currentTestWord$, "^[a-zA-Z]+'toneOne$'$") > 0 and toneTwo$ = "-"
            feedbackText$ = toneText$
        elsif rindex_regex(currentTestWord$, "^[a-zA-Z]+'toneOne$'[a-zA-Z]+'toneTwo$'$") > 0
            feedbackText$ = toneText$
        elsif toneOne$ = "Wrong"
            recognitionText$ = recognitionText$ + " ('toneText$')"
        endif
    endfor
# Correct
else
    recognitionText$ = recognitionText$ + choiceText$
    for i from 1 to numberOfFeedbackRows
        select Table ToneFeedback
        toneOne$ = Get value... 'i' T1
        toneTwo$ = Get value... 'i' T2
        toneText$ = Get value... 'i' Feedback

        if toneOne$ = "Correct"
            feedbackText$ = toneText$
        endif
    endfor
endif

# Write out result
recognitionText$ > feedback.txt
newline$ >> feedback.txt
feedbackText$ >> feedback.txt
newline$ >> feedback.txt

# Log files
currentDate$ = date$()
timeStamp$ = replace_regex$(currentDate$, "[^a-zA-Z0-9\-_]", "-", 0)
if logPerformance
   outfilename$ = "'currentTestWord$'_'choiceReference$'_'upperRegisterInput'_'timeStamp$'.wav"
   fileappend log/logFile.txt 'result$''tab$''upperRegisterInput'Hz'tab$''currentDate$''tab$''outfilename$''newline$'
   select Sound Source
   Write to WAV file... log/'outfilename$'
endif
# printline 'result$'
result$ > lastResult.txt
newline$ >> lastResult.txt

# Clean up
select Table ToneFeedback
Remove

if example$ = "Replay"
   call newPitchOnSound Source SourcePitch 'currentTestWord$'
   select Sound lastExample
   Play
elsif example$ = "Hum"
   select Pitch 'currentTestWord$'
   To Sound (hum)
   Rename... lastExample
   Play
else
   select Sound Source
   Copy... lastExample
endif

select Sound lastExample
   Write to WAV file... lastExample.wav

   # Show pitch tracks
   if example$ <> "none"
       freqTop = 1.5 * upperRegisterInput
       Erase all
       Select outer viewport... 0 6 0 4

       select Pitch SourcePitch
       Line width... 1
       Red
       Draw... 0 0 0 'freqTop' 1
       Line width... 3
       Draw... 0 0 0 'freqTop' 0

       select Pitch 'currentTestWord$'
       Green
       Draw... 0 0 0 'freqTop' 0

       24
       Text top... no 'currentTestWord$'
   endif

select Pitch SourcePitch
Write to short text file... ../records/'currentTestWord$'.Pitch
Down to PitchTier
#Moet nog wat anders worden!
Write to short text file... ../records/'currentTestWord$'.PitchTier
Remove


    select Sound Source
    plus Pitch SourcePitch
    plus Pitch 'currentTestWord$'
    plus Sound lastExample
    Remove

# Replace source pitch with 
# Names of PitchTiers
procedure newPitchOnSound sourceSound$ sourceName$ correctName$
    # Align generated pitch with source
    select Pitch 'sourceName$'
    thisDuration = Get total duration
    Copy... sourceVoiced
    numberOfFrames = Get number of frames
    numberOfVoicedFrames = Count voiced frames
    declineFactor = 1
    if numberOfVoicedFrames > 0
        declineFactor = 0.5**(1/numberOfVoicedFrames)
    endif
    Formula... if self > 0 then if self[col-1]<=0 then 200 else self[col -1]*declineFactor endif else self endif
    
    select Pitch 'correctName$'
    thisDuration = Get total duration
    Copy... spokenVoiced
    numberOfFrames = Get number of frames
    numberOfVoicedFrames = Count voiced frames
    declineFactor = 1
    if numberOfVoicedFrames > 0
        declineFactor = 0.5**(1/numberOfVoicedFrames)
    endif
    Formula... if self > 0 then if self[col-1]<=0 then 200 else self[col -1]*declineFactor endif else self endif
    
    select Pitch sourceVoiced
    plus Pitch spokenVoiced

    noprogress To DTW...  24 10 yes yes no restriction
    Rename... dtw
    Find path... yes yes no restriction

    select Pitch sourceVoiced
    plus Pitch spokenVoiced
    Remove

    # Construct PitchTiers
    select Pitch 'correctName$'
    Down to PitchTier

    select Pitch 'sourceName$'
    duration = Get total duration
    Create PitchTier... CorrectPitch 0.0 'duration'

    select PitchTier 'correctName$'
    numberOfPoints = Get number of points
    for i from 1 to numberOfPoints 
	    select PitchTier 'correctName$'
	    time = Get time from index... 'i'
	    pitch = Get value at index... 'i'
	    select DTW dtw
	    newtime = Get time along path... 'time'
	    select PitchTier CorrectPitch
	    Add point... 'newtime' 'pitch'
    endfor

    # Generate Manipulation and replace the PitchTier
    select Sound 'sourceSound$'
    noprogress To Manipulation... 0.05 60 600
    select Manipulation 'sourceSound$'
    plus PitchTier CorrectPitch
    Replace pitch tier
    select Manipulation 'sourceSound$'
    Get resynthesis (PSOLA)
    Rename... lastExample

    # Clean up
    select PitchTier CorrectPitch
    plus PitchTier 'correctName$'
    plus DTW dtw
    plus Manipulation Source
    Remove
endproc
