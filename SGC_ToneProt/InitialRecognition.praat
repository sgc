#! praat
#
# Load all CoG reference files and compare the recorded test sound to them
# Chose the reference file with the lowest distance
# 'exclude$' contains a regexp pattern that deselects unwanted reference files
# The number of coefficients should match those in the reference files
# (which should be readable from a Praat script, but aren't)
#
#   word pinyin duo1
#	word test_word wav/duo1shao3/duo1shao3_duo1shao3_F20DUTB1BS01_2006-12-11T2-00.wav
#	word reference_dir ../wordlists/CoGMandarinSounds
#   

form Give input
	word pinyin cha2
	word test_word ../../test/cha2_cha2_252_Sat-Mar-17-22-08-22-2007.wav
	word reference_dir ../wordlists/CoGMandarinSounds
endform

referenceCOGExt$ = "cog"

# Read the procedure to calculate the CoG
# Note that we preserve the initial but silence any "noise" at the end ot the final
include CoGcalculation.praat

# Bias Z-normalized value of the distance difference between smallest and correct
biasDistance = 0.9

# Debugging
#keepIntermediates = 0
debug = 0

final$ = replace_regex$(pinyin$, "^([^uoaeiv]*)([uoaeiv]+[ngmr]*[0-9])([a-zA-Z0-9]*)$", "\2", 0)
initial$ = replace_regex$(pinyin$, "^([^uoaeiv]*)([uoaeiv]+[ngmr]*[0-9])([a-zA-Z0-9]*)$", "\1", 0)
secondSyll$ = " "
if rindex_regex(pinyin$, "^'initial$''final$'([a-zA-Z]+[0-9])") > 0
    secondSyll$ = replace_regex$(pinyin$, "^'initial$''final$'([a-zA-Z]+[0-9])", "\1", 0)
endif

# printline 'initial$'+'final$'+'secondSyll$'

# Read input
if test_word$ <> "" and test_word$ <> "REUSEMFCC"
    Read from file... 'test_word$'
    Rename... Original
    Resample... 16000 50
    Rename... Source
    select Sound Original
    Remove
    select Sound Source
endif

# CoG
nasals$ = "(m|n|ng)"
plosives$ = "([ptkbdg])"
fricatives$ = "([fsxh]|sh)"
affricates$ = "([zcqj]|zh|ch)"
semivowels$ = "([ywlr])"

isNasal = 0
isPlosive = 0
isFricative = 0
isAffricate = 0
isSemivowel = 0
isEmpty = 0

# Determine manner of articulation and adapt bias
if rindex_regex(pinyin$, "^'nasals$'") > 0
    isNasal = 1
    biasDistance = 0.9
elsif rindex_regex(pinyin$, "^'plosives$'") > 0
    isPlosive=1
    biasDistance = 0.9
elsif rindex_regex(pinyin$, "^'fricatives$'") > 0
    isFricative=1
    biasDistance = 0.9
elsif rindex_regex(pinyin$, "^'affricates$'") > 0
    isAffricate=1
    biasDistance = 0.9
elsif rindex_regex(pinyin$, "^'semivowels$'") > 0
    isSemivowel=1
    biasDistance = 0.5
else
    isEmpty = 1
endif

Create Strings as file list... ReferenceList 'reference_dir$'/*'final$'.'referenceCOGExt$'
numberOfReferences = Get number of strings

# Convert input to CoG
if test_word$ <> "REUSEMFCC"
    select Sound Source
    call CoGcalculation
    Rename... Source
endif

inputMediaCoG = Get quantile... 0 0 0.5 Hertz

# Get final syllable
totalDuration = 0

smallestDistance=999999
countDistance = 0
sumDistance = 0
sumSqrDistance = 0
correctDistance = -1
choiceReference$ = pinyin$
for i from 1 to numberOfReferences
    select Strings ReferenceList
    inFile$ = Get string... 'i'
    referenceName$ = replace_regex$(inFile$, "([^.]+)."+referenceCOGExt$+"$", "\1", 0)
        
    useReference = 0
    # Special cases first!
    if initial$ = "g" and rindex_regex(inFile$, "^h'final$'") > 0
        useReference = 1
    elsif initial$ = "x" and rindex_regex(inFile$, "^k'final$'") > 0
        useReference = 1
    elsif (isNasal and rindex_regex(inFile$, "^'nasals$''final$'") > 0)
        useReference = 1
    elsif (isFricative and rindex_regex(inFile$, "^'fricatives$''final$'") > 0)
        useReference = 1
    elsif (isPlosive and rindex_regex(inFile$, "^'plosives$''final$'") > 0)
        useReference = 1
    elsif (isAffricate and rindex_regex(inFile$, "^'affricates$''final$'") > 0)
        useReference = 1
    elsif (isSemivowel and rindex_regex(inFile$, "^'semivowels$''final$'") > 0)
        useReference = 1
    elsif (isEmpty) and rindex_regex(inFile$, "^'final$'") > 0
        useReference = 1
    endif
    
    if useReference > 0
	    Read from file... 'reference_dir$'/'inFile$'

       currentMediaCoG = Get quantile... 0 0 0.5 Hertz
        factorMedian = inputMediaCoG/currentMediaCoG
        Formula... self*factorMedian

        select Pitch 'referenceName$'
        plus Pitch Source
        noprogress To DTW... 24.0 10.0 yes yes no restriction
        Rename... DTW'i'
        distance = Get distance (weighted)

        countDistance = countDistance + 1
        sumDistance = sumDistance + distance
        sumSqrDistance = sumSqrDistance + distance^2

        if distance < smallestDistance
            smallestDistance = distance
            choiceReference$ = referenceName$
        endif

        if referenceName$ = pinyin$
            correctDistance = distance
        endif
if debug > 0
    logline$ = "'pinyin$' 'inFile$' 'referenceName$' 'distance' 'smallestDistance' 'choiceReference$'"
    printline 'logline$'
    logline$ = logline$+newline$
    logline$ >> initialrecognitionlog.txt
endif

        # Clean up
        select DTW DTW'i'
	    plus Pitch 'referenceName$'
        Remove
    endif
endfor

zDistance = -1
if countDistance > 2
    meanDistance = sumDistance / countDistance
    varDistance = (sumSqrDistance - sumDistance^2/countDistance)/(countDistance - 1)
    stdDistance = sqrt(varDistance)
    diffDistance = correctDistance - smallestDistance
    zDistance = diffDistance/stdDistance

    if zDistance < biasDistance
        choiceReference$ = pinyin$
        smallestDistance = correctDistance
    endif
        
endif

# Special cases
if initial$ = "d"  and rindex_regex(choiceReference$, "^g'final$'") > 0
    choiceReference$ = "g'final$'"
endif

if debug > 0
    logline$ = "CoG Match: 'pinyin$' <== 'choiceReference$' corr='correctDistance' Z='zDistance'"
    logline$ = logline$+newline$
    logline$ >> initialrecognitionlog.txt
endif
choiceReference$ > lastInitialRecognitionResult.txt

# Clean up

select Strings ReferenceList
if test_word$ <> "" and test_word$ <> "REUSEMFCC"
    plus Sound Source
endif
plus Pitch Source
Remove


