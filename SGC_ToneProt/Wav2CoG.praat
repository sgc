#! praat
#
# Bulk conversion of WAV to CoG files
# Only converts files that do not yet exist in the target directory

form Convert sounds to CoG
    word source_dir ../wordlists/MandarinSounds
    word target_dir ../wordlists/MandarinSounds/cog
endform

sourceExt$ = "wav"
targetExt$ = "cog"

# clearinfo
include CoGcalculation.praat

Create Strings as file list... SourceList 'source_dir$'/*.'sourceExt$'
numberOfFiles = Get number of strings

#    printline 'source_dir$'/*.'sourceExt$' 'numberOfFiles' 

for i from 1 to numberOfFiles
    select Strings SourceList
    inFile$ = Get string... 'i'
    outputFile$ = replace_regex$(inFile$, sourceExt$+"$", targetExt$, 1)

    if 1 or not fileReadable("'target_dir$'/'outputFile$'")
        Read from file... 'source_dir$'/'inFile$'
        Rename... Source
        call CoGcalculation
        Rename... Source

        Write to short text file... 'target_dir$'/'outputFile$'

        # Clean up
        select Sound Source
        plus Pitch Source
        Remove
    endif
endfor
select Strings SourceList
Remove
