#!/bin/bash
intltool-extract --type=gettext/glade ../sgc.glade
xgettext -j --language=C --keyword=_ --keyword=N_ --output=sgc.pot ../*.c ../sgc.glade.h
#msginit --input=sgc.pot --locale=nl_NL
#msgmerge --output-file=nl.po nl-old.po nl.po
msgfmt --output-file=../locale/nl_NL/LC_MESSAGES/sgc.mo nl.po
