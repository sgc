SpeakGood<language> Language packs

Language packs are scripts, programs and other resources that allow
the SpeakGood<language> to be used to learn tones in a specific language.

Please put all resources into a single directory named after the language. If
possible, use the official name of the language.
