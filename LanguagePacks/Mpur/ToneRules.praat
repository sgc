# The rules for constructing the tone contours.
#
# This procedure works because in Praat, there is no namespace
# separation. All variables behave as if they are global
# (which they are).
#
# toneSyllable is the tone number on the current syllable
# 1-4, 6=Dutch (garbage) intonation
#
# These procedures set the following pareameters
# and use them to create a Pitch Tier:
#
# toneFactor:  Duration scale factor for current tone
# startPoint:  Start of the tone
# endPoint:    end of the tone
# lowestPoint: bottom of tone 3
# point: The time of the next pitch value in the contour
#        ONLY USE AS: point = point + <fraction> * voicedDuration
#
# The following values are given by the calling routine
# DO NOT ALTER THEM
# 
# toneSyllable: tone number on the current syllable
# nextTone: tone number of next syllable or -1
# prevTone: tone number of previous syllable or -1
# lastFrequency: end-frequency of the previous syllable
#
# topLine: the frequency of the first tone
# frequency_Range: Range of tone four (1 octave)
# voicedDuration: Duration of voiced part of syllable
# 

# Procedure to scale the duration of the current syllable
procedure toneDuration
	if toneSyllable = 1
    # Nothing
	elsif toneSyllable = 2
		toneFactor = 0.8
	elsif toneSyllable = 3
		toneFactor = 1.1
    elsif toneSyllable = 4
		toneFactor = 0.8
	endif
    
endproc

# DO NOT CHANGE toneFactor BELOW THIS POINT

# Rules to create a tone
#
# Do not mess with the 'Add point...' commands
# unless you know what you are doing
# The 'point =' defines the time of the next pitch value
#
# start * ?Semit is a fall
# start / ?Semit is a rise
#
procedure toneRules
    # First tone
	if toneSyllable = 1
        # Just a straight line
        startPoint = topLine
        endPoint = topLine
        
        # Handle coarticulation
        if nextTone = 1
        elsif nextTone = 2
        elsif nextTone = 3
        elsif nextTone = 4
        else
        endif
        if prevTone = 1
            # Two first tones, make them a little different
            startPoint = startPoint * 0.999
            endPoint = endPoint * 0.999
        elsif prevTone = 2
        elsif prevTone = 3
        elsif prevTone = 4
        else
        endif
    
        # Write points
        Add point... 'point' 'startPoint'
        point = point + voicedDuration
        Add point... 'point' 'endPoint'
    # Second tone
	elsif toneSyllable = 2
        # Start halfway of the range + 1 semitone
        startPoint = topLine * sqrt(frequency_Range) / oneSemit
        midPoint = startPoint
        # End 1 semitones above the first tone
        endPoint = topLine / oneSemit
        
        # Handle coarticulation
        if nextTone = 1
        elsif nextTone = 2
        elsif nextTone = 3
        elsif nextTone = 4
        else
        endif
        if prevTone = 1
        elsif prevTone = 2
        # Two consecutive tone 2, start 1 semitone higher
		    startPoint = startPoint / oneSemit
        elsif prevTone = 3
        elsif prevTone = 4
        else
        endif
    
        # Write points
        Add point... 'point' 'startPoint'
        # Next point a 1/3th of duration
        point = point + (voicedDuration)/3
        Add point... 'point' 'midPoint'
        # Remaining duration
        point = point + (voicedDuration)*2/3
        Add point... 'point' 'endPoint'
    # Third tone
	elsif toneSyllable = 3
        # Halfway the range
        startPoint = topLine * sqrt(frequency_Range)
        midPoint = startPoint * twoSemit
        endPoint = midPoint

        # Handle coarticulation
        if nextTone = 1
        elsif nextTone = 2
        elsif nextTone = 3
        elsif nextTone = 4
        else
        endif
        if prevTone = 1
        elsif prevTone = 2
        elsif prevTone = 3
        elsif prevTone = 4
        else
        endif
    
        # Write points
     	Add point... 'point' 'startPoint'
        # 
        # Next point a 2/3th of duration
        point = point + (voicedDuration)*2/3
        Add point... 'point' 'midPoint'
        # Remaining duration
        point = point + (voicedDuration)/3
        Add point... 'point' 'endPoint'
	elsif toneSyllable = 4
        # Halfway the range
        startPoint = topLine * sqrt(frequency_Range)
        lowestPoint = topLine * frequency_Range * threeSemit
        # Protect pitch against "underflow"
        if lowestPoint < absoluteMinimum
            lowestPoint = absoluteMinimum
        endif
        endPoint = lowestPoint
        
        # Handle coarticulation
        if nextTone = 1
        elsif nextTone = 2
        elsif nextTone = 3
        elsif nextTone = 4
        else
        endif
        if prevTone = 1
        elsif prevTone = 2
        elsif prevTone = 3
        elsif prevTone = 4
        else
        endif
    
        # Write points
     	Add point... 'point' 'startPoint'
        # 
        # Next point a 2/3th of duration
        point = point + (voicedDuration)*2/3
        Add point... 'point' 'lowestPoint'
        # Remaining duration
        point = point + (voicedDuration)/3
        Add point... 'point' 'endPoint'

    # Dutch intonation
	else
        # Start halfway of the range
        startPoint = topLine * sqrt(frequency_Range)
        # Or continue from last Dutch "tone"
        if prevTone = 6
            startPoint = lastFrequency
        endif
        # Add declination
        endPoint = startPoint * oneSemit
        
        # Write points
        Add point... 'point' 'startPoint'
        point = point + voicedDuration
        Add point... 'point' 'endPoint'
	endif
endproc
