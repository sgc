#! praat
#
#     SpeakGoodMpur: ToneScript.praat generates synthetic tone contours
#     for Mandarin Mpur
#     Copyright (C) 2007  R.J.J.H. van Son
#     The SpeakGoodChinese team are:
#     Guangqin Chen, Zhonyan Chen, Stefan de Konink, Eveline van Hagen, 
#     Rob van Son, Dennis Vierkant, David Weenink
#
#     SpeakGoodMpur is developed by:
#     Cecilia Ode and Rob van Son
# 
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
form Enter transcr and tone 1 frequency
	word inputWord ba1ba1
	positive upperRegister_(Hz) 300
    real range_Factor 1
    real durationScale 1
    optionmenu generate 1
        option Pitch
        option Sound
        option CorrectPitch
        option CorrectSound
endform

# To supress the ToneList, change to 0
createToneList = 1
if rindex_regex(generate$, "Correct") > 0
	createToneList = 0
endif

# Limit lowest tone
absoluteMinimum = 80

prevTone = -1
nextTone = -1

point = 0
lastFrequency = 0

# Clean up input
if inputWord$ <> ""
    inputWord$ = replace_regex$(inputWord$, "^\s*(.+)\s*$", "\1", 1)
endif

procedure extractTone syllable$
	toneSyllable = -1
	currentToneText$ = replace_regex$(syllable$, "^[^\d]+([\d]+)(.*)$", "\1", 0)
	toneSyllable = extractNumber(currentToneText$, "")
endproc

procedure convertVoicing voicingSyllable$
	# Remove tones
	voicingSyllable$ = replace_regex$(voicingSyllable$, "^([^\d]+)[\d]+", "\1", 0)
	# Convert voiced consonants
	voicingSyllable$ = replace_regex$(voicingSyllable$, "(ng|[wrlmny])", "C", 0)
	# Convert unvoiced consonants
	voicingSyllable$ = replace_regex$(voicingSyllable$, "(sh|ch|zh|[fsxhktpgqdbzcj])", "U", 0)
	# Convert vowels
	voicingSyllable$ = replace_regex$(voicingSyllable$, "([aiuoe�])", "V", 0)
endproc

# Add a tone movement. The current time point is 'point'
delta = 0.0000001
if durationScale <= 0
    durationScale = 1.0
endif
segmentDuration = 0.150
fixedDuration = 0.12

#
# Movements
# start * ?Semit is a fall
# start / ?Semit is a rise
# 1/(12 semitones)
octave = 0.5
# 1/(9 semitones)
nineSemit = 0.594603557501361
# 1/(6 semitones)
sixSemit = 0.707106781186547
# 1/(3 semitones) down
threeSemit = 0.840896415253715
# 1/(2 semitones) down
twoSemit = 0.890898718140339
# 1/(1 semitones) down
oneSemit = 0.943874313
# 1/(4 semitones) down
fourSemit = twoSemit * twoSemit
# 1/(5 semitones) down
fiveSemit = threeSemit * twoSemit

frequency_Range = octave
if range_Factor > 0
    frequency_Range =  frequency_Range * range_Factor
endif

# Get the rules of the tones
include ToneRules.praat

# Previous end frequency
lastFrequency = 0
procedure addToneMovement syllable$ topLine prevTone nextTone
	# Get tone
	toneSyllable = -1
	call extractTone 'syllable$'

    # This would be the place to execute obligatory sandhi rules
    # like the Mandarin 3-3 => 2-3 rule using toneSyllable and nextTone

	# Get voicing pattern
	voicingSyllable$ = ""
	call convertVoicing 'syllable$'

	# Account for tones in duration
	toneFactor = 1
    # Scale the duration of the current syllable
    call toneDuration
	toneFactor = toneFactor * durationScale

	# Unvoiced part
	if rindex_regex(voicingSyllable$, "U") = 1
		point = point + delta
        Add point... 'point' 0
		point = point + segmentDuration * toneFactor
        Add point... 'point' 0
	endif
	# Voiced part
	voiceLength$ = replace_regex$(voicingSyllable$, "U*([CV]+)U*", "\1", 0)
	voicedLength = length(voiceLength$)
	voicedDuration = toneFactor * (segmentDuration*voicedLength + fixedDuration)
	point = point + delta

    # Write contour of each tone
    # Note that tones are influenced by the previous (tone 0) and next (tone 3)
    # tones. Tone 6 is the Dutch intonation
    # sqrt(frequency_Range) is the mid point
    if topLine * frequency_Range < absoluteMinimum
        frequency_Range = absoluteMinimum / topLine
    endif

    call toneRules
	
    lastFrequency = endPoint

endproc

# Split input into syllables
margin = 0.25

procedure wordToTones wordInput$ highPitch
	currentRest$ = wordInput$;
	syllableCount = 0
	length = 2 * margin

    # Split syllables
	while rindex_regex(currentRest$, "^[^\d]+[\d]+") > 0
        syllableCount += 1
        syllable'syllableCount'$ = replace_regex$(currentRest$, "^([^\d]+[\d]+)(.*)$", "\1", 1)
		currentSyllable$ = syllable'syllableCount'$

		# Get the tone
		call extractTone 'currentSyllable$'
		toneSyllable'syllableCount' = toneSyllable
		currentTone = toneSyllable'syllableCount'

		# Get the Voicing pattern
		call convertVoicing 'currentSyllable$'
		voicingSyllable'syllableCount'$ = voicingSyllable$
		currentVoicing$ = voicingSyllable'syllableCount'$

		# Calculate new length
	    # Account for tones in duration
	    toneFactor = 1
        # Scale the duration of the current syllable
        call toneDuration
	    toneFactor = toneFactor * durationScale

		length = length + toneFactor * (length(voicingSyllable'syllableCount'$) * (segmentDuration + delta) + fixedDuration)

		# Next round
		currentRest$ = replace_regex$(currentRest$, "^([^\d]+[\d]+)(.*)$", "\2", 1)

		# Safety valve
		if syllableCount > 2000
   			exit
		endif
	endwhile

	# Create tone pattern
	Create PitchTier... 'wordInput$' 0 'length'

	# Add start margin
	lastFrequency = 0
    point = 0
	Add point... 'point' 0
	point = margin
	Add point... 'point' 0

    lastTone = -1
    followTone = -1
	for i from 1 to syllableCount
		currentSyllable$ = syllable'i'$
        currentTone = toneSyllable'i'
        followTone = -1
        if i < syllableCount
            j = i+1
            followTone = toneSyllable'j'
        endif

		call addToneMovement 'currentSyllable$' 'highPitch' 'lastTone' 'followTone'

        lastTone = currentTone
	endfor

	# Add end margin
	point = point + delta
	Add point... 'point' 0
	point = point + margin
	Add point... 'point' 0
endproc

procedure generateWord whatToGenerate$ theWord$ upperRegister
	call wordToTones 'theWord$' 'upperRegister'
	# Generate pitch
    select PitchTier 'theWord$'
    noprogress To Pitch... 0.0125 60.0 600.0
	Rename... theOrigWord
	Smooth... 10
	Rename... 'theWord$'
	select Pitch theOrigWord
	Remove

    # Generate sound if wanted
    select Pitch 'theWord$'
    if rindex_regex(whatToGenerate$, "Sound") > 0
	    noprogress To Sound (hum)
    endif

    # Clean up
    select PitchTier 'theWord$'
    if rindex_regex(whatToGenerate$, "Sound") > 0
        plus Pitch 'theWord$'
    endif
    Remove
endproc

# Get a list of items
if createToneList = 1
    Create Table with column names... ToneList 36 Word

    for i from 1 to 36
	    select Table ToneList
        Set string value... 'i' Word ------EMPTY
    endfor
endif

syllableCount = length(replace_regex$(inputWord$, "[^\d]+([\d]+)", "1", 0))
wordNumber = 0
lowerBound = 1
if syllableCount = 1
     lowerBound = 1
endif
if rindex(generate$, "Correct") <= 0
    for first from lowerBound to 6
	    currentWord$ = replace_regex$(inputWord$, "^([^\d]+)([\d]+)(.*)$", "\1'first'\3", 1)
	    for second from 1 to 6
		    if (first <> 5 and second <> 5) and (syllableCount > 1 or second == 1)
			    currentWord$ = replace_regex$(currentWord$, "^([^\d]+)([\d]+)([^\d]+)([\d]+)$", "\1'first'\3'second'", 1)
                # Write name in list
                wordNumber = wordNumber+1
                if createToneList = 1
	                select Table ToneList
                    listLength = Get number of rows
                    listLength = listLength + 1
                    for currLength from listLength to wordNumber
                        Append row
                        Set string value... 'currLength' Word ------EMPTY
                    endfor
                    Set string value... 'wordNumber' Word 'currentWord$'
                endif

                # Actually, generate something
                call generateWord 'generate$' 'currentWord$' 'upperRegister'
		    endif
	    endfor
    endfor
else
    call generateWord 'generate$' 'inputWord$' 'upperRegister'
endif
