SpeakGoodMpur tone recongizer

Mpur is a minority (endangered) Papua language spoken in the
Birds-Head region of West Irian (Iriyan Jaya)

This part was developed by Cecilia Ode and rob van Son


TONE RECOGNIZER
ToneScript.praat           : Tone generator text -> Tone model
ToneRules.praat            : "Rules" that determine tone shapes and duration

OTHERS
feedback                   : Directory with the language specific feedback texts
README.txt                 : This file
